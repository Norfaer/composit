<div class="bs-root">
    <div class="container mt-5">        
        <form class="form" action="<?=$ajax_url?>" method="post" id="wpci-settings-form">
            <input type="hidden" name="action" value="wpci_save_settings">
            <?php wp_nonce_field('wpci_save_settings')?>
            <div class="row">
                <div class="col-sm-4">
                    <h2><?= __('Settings Page', 'wpci')?></h2>
                </div>
                <div class="col-sm-8">
                    <label id="wpci-config-upload" data-nonce="<?=wp_create_nonce('wpci_upload_config')?>" data-referer="<?=esc_attr( wp_unslash( $_SERVER['REQUEST_URI'] ) )?>" class="btn btn-success mb-0" type="button" role="button"><i class="fa fa-upload"></i> <?= __('Upload config file', 'wpci')?><input type="file" name="config_file" hidden></label>
                    <button class="btn btn-warning async" id="wpci-config-refresh" data-nonce="<?=wp_create_nonce('wpci_update_config')?>" data-referer="<?=esc_attr( wp_unslash( $_SERVER['REQUEST_URI'] ) )?>"  type="button" role="button"><span class="processing"><i class="fa fa-refresh fa-spin"></i> <?= __('Reloading config file...', 'wpci')?></span><span class="regular"><i class="fa fa-refresh"></i> <?= __('Reload config file', 'wpci')?></span></button>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="input-google-api-key"><?= __('Google Api Key', 'wpci')?></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-key"></i></span>
                            </div>
                            <input id="input-google-api-key" type="text" value="<?=$options['gapi_key']?>" name="__wpci_options[gapi_key]" placeholder="Api Key" class="form-control" aria-describedby="input-google-api-key-description">
                        </div>                        
                        <small id="input-google-api-key-description" class="form-text text-muted"><?= __('Provide valid Google Api key in order to use google maps. In your google developer console enable the following apis: geometry and places', 'wpci')?></small>
                    </div>
                </div>                
                <div class="col-sm-12">
                    <button class="btn btn-info async" role="button" id="wpci-settings-submit" type="submit"><span class="processing"><i class="fa fa-refresh fa-spin"></i> <?= __('Saving settings...', 'wpci')?></span><span class="regular"><i class="fa fa-save"></i> <?= __('Save settings', 'wpci')?></span></button>
                </div>
            </div>
        </form>
    </div>    
    <div class="modal fade" id="modal-settings" tabindex="-1" role="dialog" aria-labelledby="modal-settings-title" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="modal-settings-title"></h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body" id="modal-settings-body"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-simple" data-dismiss="modal">Ok</button>
          </div>
        </div>
      </div>
    </div>

</div>
