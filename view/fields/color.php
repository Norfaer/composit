<div class="<?=$width?>">
    <div class="form-group">
        <label for="<?=$id?>"><?=$label?></label>
        <input autocomplete="off" id="<?=$id?>" type="text" value="<?= htmlspecialchars($value)?>" name="<?=$name?>" data-name-template="<?=$name?>" class="color-picker" aria-describedby="<?=$id.'-description'?>">
        <?php if($description): ?>
        <small id="<?=$id.'-description'?>" class="form-text text-muted"><?=$description?></small>
        <?php endif; ?>        
    </div>    
</div>
