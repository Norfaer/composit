<div class="<?=$width?> mapref-container">
    <div class="row">
        <div class="form-group col-sm-4">
            <label for="<?=$id?>-zoom"><?=__('Zoom','wpci')?>:</label>
            <input type="range" class="custom-range input-zoom" step="0.05" max="21" min="0" id="<?=$id?>-zoom" data-name-template="<?=$name?>" name="<?=$name?>[zoom]" value="<?=$value['zoom']?>">
        </div>
        <div class="form-group col-sm-4">
            <label for="<?=$id?>-centerX"><?=__('Center longitude (X)', 'wpci')?></label>
            <input type="text" id="<?=$id?>-centerX" value="<?=$value['centerx']?>" class="form-control center-x" name="<?=$name?>[centerx]">
        </div>
        <div class="form-group col-sm-4">
            <label for="<?=$id?>-centerY"><?=__('Center latitude (Y)', 'wpci')?></label>
            <input type="text" id="<?=$id?>-centerY" value="<?=$value['centery']?>"  class="form-control center-y" name="<?=$name?>[centery]">        
        </div>
    </div>
    <div class="form-group">
        <label><?=$label?></label>   
        <?php if($params['search']):?>
        <input class="input-search" type="text" placeholder="<?=__('Search For Places', 'wpci')?>">
        <?php endif; ?>
        <div class="map-container" data-search="<?=$params['search']?'1':''?>" data-country="<?=$params['country']?>" data-center-x="<?=$value['centerx']?>" data-center-y="<?=$value['centery']?>" data-zoom="<?=$value['zoom']?>"></div> 
        <small><?=__('To add marker - right click on the map, to remove it - right click on it again','wpci')?></small>
    </div>
    <input class="markers" name="<?=$name?>[markers]" value="<?=$value['markers']?>" type="hidden">
</div>