<div class="col-sm-12">
    <label><?=$label?></label>
    <div class="multi-collection" data-representation="<?=$params['representation']?>">
        <div class="sortable-container" data-name-template="<?=$name?>">
            <?php
                $index = 0;                                
                foreach ($value as $collection):
            ?>
            <div class="sortable-item" data-index="<?=$index?>">
                <div class="sortable-item-header"><span class="representation">&nbsp;</span><span class="sortable-controls"><i class="fa"></i></span></div>
                <div class="sortable-item-body">            
                    <div class="row">
                    <?php 
                        foreach ($children as $child) {
                            $child['object_id'] = $object_id;
                            $child['object'] = $object;
                            $child['value'] = isset($collection[$child['name']]) ?  $collection[$child['name']] : '';  
                            (new composit\core\Field($child))->render($object_id);                    
                        }
                    ?>
                    </div>
                    <hr>
                    <button class="btn btn-danger btn-simple btn-sm del-collection" type="button"><i class="fa fa-trash"></i> <?=__('Delete','wpci')?></button>
                </div>
            </div>
            <?php
                $index++;
                endforeach;
            ?>    
        </div>
        <div class="row">
            <div class="col-sm-12">
                <button class="add-collection btn btn-info btn-sm" type="button"><i class="fa fa-plus"> <?=__('Add element','wpci')?></i></button>
            </div>
        </div>
        <div class="sortable-item multi-collection-template">
            <div class="sortable-item-header"><span class="representation">&nbsp;</span><span class="sortable-controls"><i class="fa"></i></span></div>
            <div class="sortable-item-body">            
                <div class="row">
                <?php 
                    foreach ($children as $child) {
                        $child['object_id'] = $object_id;
                        $child['object'] = $object;
                        $child['collection'] = true;
                        (new composit\core\Field($child))->render($object_id);                    
                    }
                ?>
                </div>
                <hr>
                <button class="btn btn-danger btn-simple btn-sm del-collection" type="button"><i class="fa fa-trash"></i> <?=__('Delete','wpci')?></button>
            </div>
        </div>
    </div>
</div>
