<div class="<?=$width?>">
    <div class="form-group">
        <label for="<?=$id?>"><?= $label?></label>
        <select name="<?=$name?>" data-name-template="<?=$name?>" id="<?=$id?>" class="custom-select">
            <?php if(!empty($params['prompt'])):?>                    
                <option vlaue="" <?= selected($value, '')?>><?=$params['prompt']?></option>
            <?php endif; ?>
            <?php $index = 0; foreach($list as $row):?>
                <option value="<?=$row['key']?>" <?= selected($row['key'], $value)?>><?=$row['value']?></option>
            <?php $index++; endforeach;?>
        </select>
    </div>
</div>