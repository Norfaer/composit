<div class="<?=$width?>">
    <div class="form-group">
        <label for="<?=$id?>"><?=$label?></label>
        <div class="input-group">
            <input autocomplete="off" <?=$params['required'] ? 'data-input-required':''?> id="<?=$id?>" type="text" class="form-control date-picker" data-date-format="<?=$params['time'] ? $params['dateFormat'] . ' hh:mm:ss' : $params['dateFormat']?>" name="<?=$name?>" placeholder="<?=$params['placeholder']?>" data-name-template="<?=$name?>" value="<?=$value?>"  aria-describedby="<?=$id.'-description'?>">
            <div class="input-group-append" data-target="#<?=$id?>" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>            
        </div>
        <small id="<?=$id.'-description'?>" class="form-text text-muted"><?=$description?></small>
    </div>
</div>