<div class="<?=$width?>">
    <div class="form-group">
        <label><?=$label?></label>
        <div class="wpci-image-select wpci-image-singleselect" data-placeholder="<?=$params['placeholder']?>">
            <input type="hidden" name="<?= $name ?>" data-name-template="<?=$name?>" value="<?=$value?>">
            <?php if($value): 
                    $type = get_post_mime_type((int)$value);                
                    switch($type):
                        case 'video/mpeg':
                        case 'video/mp4': 
                        case 'video/quicktime':
                            $icon = wp_mime_type_icon( (int)$value );
                            $filename = basename(get_attached_file((int)$value));
            ?>
                <div class="image-block" style="background: url('<?=$icon?>') no-repeat center center;" data-image-id="<?=$value?>">
                    <div class="preview-header"><?=$filename?></div>
                    <div class="image-block-tools">
                        <a class="btn btn-primary btn-sm" href="<?= admin_url('post.php?post='.$value.'&action=edit&image-editor')?>" target="_blank"><i class="fa fa-pencil"></i></a>
                        <a class="btn btn-danger btn-sm tool-button-delete" href="#"><i class="fa fa-trash"></i></a>
                    </div>
                </div>            
            <?php
                            break;
                        case 'image/jpeg':
                        case 'image/png':
                        case 'image/gif':
                        default:
            ?>
                <div class="image-block" style="background-image: url('<?= wp_get_attachment_url($value)?>')" data-image-id="<?=$value?>">
                    <div class="image-block-tools">
                        <a class="btn btn-primary btn-sm" href="<?= admin_url('post.php?post='.$value.'&action=edit&image-editor')?>" target="_blank"><i class="fa fa-pencil"></i></a>
                        <a class="btn btn-danger btn-sm tool-button-delete" href="#"><i class="fa fa-trash"></i></a>
                    </div>
                </div>            
            <?php
                            break;
                    endswitch;
            ?>
            <?php else:?>
                <div class="image-placeholder">
                    <div class="image-placeholder-background">
                        <div class="placeholder-text"><?=$params['placeholder']?></div>
                    </div>
                </div>        
            <?php endif; ?>        
        </div>   
    </div>
</div>