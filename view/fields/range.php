<div class="<?=$width?>">
    <div class="form-group">
        <label for="<?=$id?>"><?=$label?></label>
        <input type="range" class="custom-range" step="<?=$params['step']?>" max="<?=$params['to']?>" min="<?=$params['from']?>" id="<?=$id?>" data-name-template="<?=$name?>" name="<?=$name?>" value="<?=$value?>" aria-describedby="<?=$id.'-description'?>">
        <small id="<?=$id.'-description'?>" class="form-text text-muted"><?=$description?></small>
    </div>
</div>
