<div class="<?=$width?>">
    <div class="form-group">
        <label for="<?=$id?>"><?=$label?></label>
        <?php if($params['icon']): ?>
        <div class="input-group">
            <input autocomplete="off" <?=$params['required'] ? 'data-input-required':''?> id="<?=$id?>" type="text" value="<?= htmlspecialchars($value)?>" name="<?=$name?>" data-name-template="<?=$name?>" data-placeholder="<?=$params['placeholder']?>" data-pattern="<?=$params['pattern']?>" class="form-control i18n-multilingual input-mask" aria-describedby="<?=$id.'-description'?>">
            <div class="input-group-append">
                <span class="input-group-text"><i class="fa <?=$params['icon']?>"></i></span>
            </div>
        </div>
        <?php else: ?>
        <input autocomplete="off" id="<?=$id?>" <?=$params['required'] ? 'data-input-required':''?> type="text" value="<?= htmlspecialchars($value)?>" name="<?=$name?>" data-name-template="<?=$name?>" data-placeholder="<?=$params['placeholder']?>" data-pattern="<?=$params['pattern']?>" class="form-control i18n-multilingual input-mask" aria-describedby="<?=$id.'-description'?>">
        <?php endif;?>
        <?php if($description): ?>
        <small id="<?=$id.'-description'?>" class="form-text text-muted"><?=$description?></small>
        <?php endif; ?>
    </div>
</div>