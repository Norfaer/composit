<div class="<?=$width?>">
    <div class="form-group" data-name-template="<?= $name?>">
        <label><?=$label?></label>
        <div class="wpci-image-select wpci-image-multiselect">
            <ul class="row image-sortable-list">
                <li class="image-placeholder">
                    <div class="image-placeholder-background">
                        <div class="placeholder-text"><?=$params['placeholder']?></div>                        
                    </div>
                </li>
                <?php foreach($value as $image_id):                     
                    $type = get_post_mime_type((int)$image_id);
                    switch($type):
                        case 'video/mpeg':
                        case 'video/mp4': 
                        case 'video/quicktime':
                            $icon = wp_mime_type_icon( (int)$image_id );
                            $filename = basename(get_attached_file((int)$image_id));
                        ?>
                            <li class="image-block" style="background: url('<?=$icon?>') no-repeat center center;" data-image-id="<?=$image_id?>">
                                <div class="preview-header"><?=$filename?></div>
                                <div class="image-block-tools">
                                    <a class="btn btn-primary btn-sm" href="<?= admin_url('post.php?post='.$image_id.'&action=edit&image-editor')?>" target="_blank"><i class="fa fa-pencil"></i></a>
                                    <a class="btn btn-danger btn-sm tool-button-delete" href="#"><i class="fa fa-trash"></i></a>
                                </div>
                            </li>            
                        <?php
                            break;
                        case 'image/jpeg':
                        case 'image/png':
                        case 'image/gif':
                        default:
                        ?>
                            <li class="image-block" style="background-image: url('<?= wp_get_attachment_url($image_id)?>')" data-image-id="<?=$image_id?>">
                                <div class="image-block-tools">
                                    <a class="btn btn-primary btn-sm" href="<?= admin_url('post.php?post='.$image_id.'&action=edit&image-editor')?>" target="_blank"><i class="fa fa-pencil"></i></a>
                                    <a class="btn btn-danger btn-sm tool-button-delete" href="#"><i class="fa fa-trash"></i></a>
                                </div>
                            </li>            
                        <?php
                            break;
                    endswitch;                    
                ?>                                                               
                <?php endforeach; ?>
            </ul>
            <input type="hidden" name="<?= $name?>" value="<?= implode(',', $value)?>">
        </div>   
    </div>
</div>