<div class="<?=$width?>">
    <div class="form-group">
        <label for="<?=$id?>"><?=$label?></label>
        <?php if($params['icon']): ?>
        <div class="input-group">
            <input autocomplete="off" <?=$params['validator'] ? 'data-input-pattern="'.$params['validator'].'"' : ''?> <?=$params['required'] ? 'data-input-required':''?> <?=$params['type'] ? 'data-input-' . $params['type'] :''?> id="<?=$id?>" type="<?=$params['type']?>" value="<?= htmlspecialchars($value)?>" name="<?=$name?>" data-name-template="<?=$name?>" placeholder="<?=$params['placeholder']?>" class="form-control i18n-multilingual" aria-describedby="<?=$id.'-description'?>">
            <div class="input-group-append">
                <span class="input-group-text"><i class="fa <?=$params['icon']?>"></i></span>
            </div>
        </div>
        <?php else: ?>
        <input autocomplete="off" <?=$params['validator'] ? 'data-input-pattern="'.$params['validator'].'"' : ''?> <?=$params['required'] ? 'data-input-required':''?> <?=$params['type'] ? 'data-input-' . $params['type'] :''?> id="<?=$id?>" type="<?=$params['type']?>" value="<?= htmlspecialchars($value)?>" name="<?=$name?>" data-name-template="<?=$name?>" placeholder="<?=$params['placeholder']?>" class="form-control i18n-multilingual" aria-describedby="<?=$id.'-description'?>">
        <?php endif;?>
        <?php if($description): ?>
        <small id="<?=$id.'-description'?>" class="form-text text-muted"><?=$description?></small>
        <?php endif; ?>
    </div>
</div>