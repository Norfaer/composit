<div class="<?=$width?>">
    <div class="form-group">
        <div class="custom-control custom-checkbox">
            <input id="<?=$id?>" class="custom-control-input" name="<?=$name?>" data-name-template="<?=$name?>" <?= checked($value, 'on')?> type="checkbox">
            <label class="custom-control-label" for="<?=$id?>">
                <?=$label?>
            </label>
        </div>
        <?php if($description): ?>
        <small id="<?=$id.'-description'?>" class="form-text text-muted"><?=$description?></small>
        <?php endif; ?>        
    </div>
</div>