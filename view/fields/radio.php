<div class="<?=$width?>">
    <div class="form-group">
        <div class="radio-header"><?=$label?></div>
        <?php $index = 0; foreach($list as $key => $row):?>
            <div class="custom-control custom-radio">
                <input class="custom-control-input" data-name-template="<?=$name?>" name="<?=$name?>" id="<?=$id . '-' . $row['key']?>" value="<?=$row['key']?>" type="radio" <?=checked($row['key'], $value)?>>
                <label class="custom-control-label" for="<?=$id . '-' . $row['key']?>">
                    <?=$row['value']?>
                </label>
            </div>            
        <?php $index++; endforeach;?>
    </div>
    <?php if($description): ?>
    <small id="<?=$id.'-description'?>" class="form-text text-muted"><?=$description?></small>
    <?php endif; ?>
</div>