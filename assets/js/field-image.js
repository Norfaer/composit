;(function($){    
    function wpciGalleryUpdate($container) {
        var ids = [];        
        $container.find('li.image-block').each(function(){
            ids.push($(this).data('image-id'));
        });
        $container.find('input[type="hidden"]').val(ids.join(','));
        //$container.append('<input type="hidden" name="' + $container.closest('.form-group').data('name-template') + '" value="' + ids.join(',') + '">');
    }
    
    $(document).ready(function(){
        var $list;
        var $frame;
        
        if ($('.wpci-image-multiselect').length) {
            var $wpmediaMulti = wp.media({
                title: 'Выберите медиа файлы',
                button: {
                    text: 'Выбрать'
                },
                library: {
                    //type: 'any'
                },
                multiple: true
            });
            $wpmediaMulti.on('select', function(){
                var selection = $wpmediaMulti.state().get('selection').toJSON();
                $list.find('li.image-block').remove();
                selection.forEach(function(elem, index){
                    if(elem.type === 'image') {
                        $list.append('<li class="image-block" style="background-image: url('+ elem.url +')" data-image-id="' + elem.id + '"><div class="image-block-tools"><a class="btn btn-primary btn-sm" href="' + Wpci.config.imageEditUrl + elem.id  + '" target="_blank"><i class="fa fa-pencil"></i></a><a href="#" type="button" class="btn btn-danger btn-sm tool-button-delete"><i class="fa fa-trash"></i></a></div></li>');                                                                                                                                       
                    }                    
                    if(elem.type === 'video') {
                        $list.append('<li class="image-block" style="background: url('+ elem.icon +') no-repeat center center;" data-image-id="' + elem.id + '"><div class="preview-header">' + elem.filename + '</div><div class="image-block-tools"><a class="btn btn-primary btn-sm" href="' + Wpci.config.imageEditUrl + elem.id  + '" target="_blank"><i class="fa fa-pencil"></i></a><a href="#" type="button" class="btn btn-sm btn-danger tool-button-delete"><i class="fa fa-trash"></i></a></div></li>');                
                    }
                });
                wpciGalleryUpdate($frame);
                $list.sortable({
                  placeholder: "image-placeholder",
                  items: 'li.image-block',
                  stop: function(){
                      wpciGalleryUpdate($(this).closest('.wpci-image-multiselect'));
                  }
                });
            });
            //        $wpmedia.on('close', function(){});
            $wpmediaMulti.on('open', function(){
                var selection = $wpmediaMulti.state().get('selection');
                selection.reset();
                
                $frame.find('input').val().split(',').forEach(function(id){
                    var attachment = wp.media.attachment(id);
                    attachment.fetch();
                    selection.add( attachment ? [ attachment ] : [] );                    
                });
                
//                $frame.find('input').each(function(){
//                    $(this).val().s
//                    var attachment = wp.media.attachment($(this).val());
//                    attachment.fetch();
//                    selection.add( attachment ? [ attachment ] : [] );
//                });
            });    
            $(document).on('click', '.wpci-image-multiselect .image-placeholder', function(e){
                e.preventDefault();
                $frame = $(this).closest('.wpci-image-multiselect');
                $list = $frame.find('ul');
                $wpmediaMulti.open();
            });
            $(document).on('click', '.wpci-image-multiselect .tool-button-delete', function(e){
                e.preventDefault();
                var container = $(this).closest('.wpci-image-multiselect');
                $(this).closest('li').remove();
                wpciGalleryUpdate(container);
            });
            $(".image-sortable-list").sortable({
              placeholder: "image-placeholder",
              items: 'li.image-block',
              stop: function(){
                wpciGalleryUpdate($(this).closest('.wpci-image-multiselect'));
              }
            });            
        }
        
        if ($('.wpci-image-singleselect').length) {
            var $wpmediaSingle = wp.media({
                title: 'Выберите медиа файл',
                button: {
                    text: 'Выбрать'
                },
                library: {
                    //type: 'any'
                },
                multiple: false
            });            
            $wpmediaSingle.on('select', function(){
                var selection = $wpmediaSingle.state().get('selection').toJSON();
                var elem = selection[0];
                $frame.find('.image-placeholder').remove();
                if(elem.type === 'image') {
                    $frame.append('<div class="image-block" style="background-image: url('+ elem.url +')" data-image-id="' + elem.id + '"><div class="image-block-tools"><a class="btn btn-primary btn-sm" href="' + Wpci.config.imageEditUrl + elem.id  + '" target="_blank"><i class="fa fa-pencil"></i></a><a href="#" type="button" class="btn btn-danger btn-sm tool-button-delete"><i class="fa fa-trash"></i></a></div></div>');                
                }
                if(elem.type === 'video') {
                    $frame.append('<div class="image-block" style="background: url('+ elem.icon +') no-repeat center center;" data-image-id="' + elem.id + '"><div class="preview-header">' + elem.filename + '</div><div class="image-block-tools"><a class="btn btn-primary btn-sm" href="' + Wpci.config.imageEditUrl + elem.id  + '" target="_blank"><i class="fa fa-pencil"></i></a><a href="#" type="button" class="btn btn-danger btn-sm tool-button-delete"><i class="fa fa-trash"></i></a></div></div>');                
                }
                $frame.find('input').val(elem.id);
            });
            $wpmediaSingle.on('open', function(){
                var selection = $wpmediaSingle.state().get('selection');
                selection.reset();
            });               
            $(document).on('click', '.wpci-image-singleselect .image-placeholder', function(e){
                e.preventDefault();
                $frame = $(this).closest('.wpci-image-singleselect');
                $wpmediaSingle.open();
            });
            $(document).on('click', '.wpci-image-singleselect .tool-button-delete', function(e){
                e.preventDefault();
                var placeholder = $(this).closest('.wpci-image-singleselect').data('placeholder');
                var container = $(this).closest('.wpci-image-singleselect');
                $(this).closest('.image-block').remove();
                container.append('<div class="image-placeholder"><div class="image-placeholder-background"><div class="placeholder-text">' + placeholder + '</div></div></div>');
                container.find('input').val(0);
            });            
        }
    });          
})(jQuery);

