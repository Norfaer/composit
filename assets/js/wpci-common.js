if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

function loadScript(src, callback)
{
  var s,r,t;
  r = false;
  s = document.createElement('script');
  s.type = 'text/javascript';
  s.src = src;
  s.onload = s.onreadystatechange = function() {
    if ( !r && (!this.readyState || this.readyState == 'complete') ) {
      r = true;
      callback();
    }
  };
  t = document.getElementsByTagName('script')[0];
  t.parentNode.insertBefore(s, t);
}

function validateForm($form) {
    var isValid = true;
    $form.find('.is-invalid').removeClass('is-invalid');
    $form.find('.invalid-feedback').remove();
    $form.find(':input').each(function(){
        var inputValid = true;
        var tab = '';
        var message = '';
        var reMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var rePhone =/^([0-9\+\- \(\)]{8,20})$/;
        var reUrl = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
        var reNumber = /^(?:\+|\-)?\d*\.?\d*$/;
        if (typeof $(this).data('input-required')!== 'undefined') {
            switch($(this).attr('type')) {
                case 'text':
                case 'email':
                case 'number':
                case 'tel':
                case 'url':
                case 'date': 
                case 'password':
                default: 
                    inputValid = !!$(this).val().length
                    break;
                case 'checkbox':
                case 'radio':
                    inputValid = !!$(this).prop('checked')
                    break;
            }                    
            message = Wpci.messages.errRequired;  
            if (!inputValid) {
                console.log('Form invalid required', this )
            }
        }
        if (inputValid && typeof $(this).data('input-custom')!== 'undefined' && (!$(this).val().match($(this).data('input-pattern')))) {
            inputValid = false;
            message = Wpci.messages.errPattern.format($(this).data('input-pattern'));
            console.log('Form invalid pattern')
        }
        if (inputValid && typeof $(this).data('input-phone')!== 'undefined' && (!$(this).val().match(rePhone))) {
            inputValid = false;
            message = Wpci.messages.errPhone;
            console.log('Form invalid phone')
        }
        if (inputValid && typeof $(this).data('input-url')!== 'undefined' && (!$(this).val().match(reUrl))) {
            inputValid = false;
            message = Wpci.messages.errUrl;
            console.log('Form invalid url')
        }
        if (inputValid && typeof $(this).data('input-number')!== 'undefined' && (!$(this).val().match(reNumber))) {
            inputValid = false;
            message = Wpci.messages.errNumber;
            console.log('Form invalid number')
        }
        if (inputValid && typeof $(this).data('input-email')!== 'undefined' && (!$(this).val().match(reMail))) {
            inputValid = false;
            message = Wpci.messages.errEmail;
            console.log('Form invalid email')
        }
        if(!inputValid) {
            isValid = false;
            $(this).addClass('is-invalid');
            $(this).parent().append('<div class="invalid-feedback">' + message +  '</div>');
            $(this).closest('.sortable-item').addClass('collapsed')
            if ($(this).closest('.tab-pane')) {
                tab = $(this).closest('.tab-pane').prop('id');
                $form.find('.nav-link[href="#'+tab+'"]').addClass('is-invalid');
            }
        }
    });
    return isValid;
}



( function( $ ) {
    $(document).ready(function() {
        //  Activate the Tooltips
        $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip();
        // Activate Popovers and set color for popovers
        $('[data-toggle="popover"]').each(function() {
            color_class = $(this).data('color');
            $(this).popover({
                template: '<div class="popover popover-' + color_class + '" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
            });
        });

        // Activate Datepicker (jQueryUI)
        $('.date-picker').each(function() {
            if ($(this).closest('.multi-collection-template').length) {
                return;
            }
            $(this).datetimepicker({
                locale: Wpci.config.locale_short,
                format: $(this).data('date-format')
            });
        });
        
        //Activate Input mask
        $('.input-mask').each(function() {
            if ($(this).closest('.multi-collection-template').length) {
                return;
            }
            $(this).inputmask($(this).data('pattern'), {
                placeholder: $(this).data('placeholder')
            })
        });
        
        // Activate Colorpicker
        $('.color-picker').each(function(){
            if ($(this).closest('.multi-collection-template').length) {
                return;
            }
            $(this).wpColorPicker( {
                width: 400,
                palettes: true
            } );
        });
        
        $('.post-ref').each(function(){
            Vue.component('Postref', window.Postref.default);
            new Vue({el: this});            
        })

        $('form#edittag, form#addtag, form#post').each(function(){
            $(this).attr('novalidate', 'novalidate');
            $(this).on('submit', function(e){
                if(!validateForm($(this))) {                
                    e.preventDefault();
                }                
            });
        });
    });
}( jQuery ));