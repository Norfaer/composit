( function( $ ) {
    $(document).ready(function() {
        $("#wpci-settings-submit").click(function(e){
            e.preventDefault();
            if ($(this).hasClass('disabled')) {
                return;
            }
            else {
                $("#wpci-settings-form").trigger('submit');
                $("#wpci-settings-submit").addClass('disabled');
            }            
        });
        
        $("#wpci-config-refresh").click(function(e){
            e.preventDefault();
            if ($(this).hasClass('disabled')) {
                return;
            }
            else {
                $("#wpci-config-refresh").addClass('disabled');                
                $.ajax({
                    url: Wpci.config.ajaxUrl,
                    dataType: 'json',
                    method: 'post',
                    data: {
                        action: "wpci_update_config",
                        _wp_http_referer: $(this).data('referer'),
                        _wpnonce: $(this).data('nonce')
                    },
                    success:function(response) {
                        $("#modal-settings-title").text('Success');                   
                        $("#modal-settings-body").text(response.data.msg);         
                        $("#modal-settings").modal('show');
                        $('#modal-settings').on('hidden.bs.modal', function () {
                            window.location.reload();
                        });
                    },
                    error:function() {
                        $("#modal-settings-title").text('Error');
                        $("#modal-settings-body").text('Error updating config');
                        $("#modal-settings").modal('show');
                    },
                    complete:function() {
                        $("#wpci-config-refresh").removeClass('disabled');
                    }
                });                
            }      
        });
        
        
        $("#wpci-config-upload input[type='file']").change(function(e){
            e.preventDefault();
            console.log('File selected');
            var file = this.files[0];
            var data = new FormData;
            data.append('action', 'wpci_upload_config');
            data.append('_wp_http_referer', $("#wpci-config-upload").data('referer'));
            data.append('_wpnonce', $("#wpci-config-upload").data('nonce'));
            data.append('config_file', file);
            if ($("#wpci-config-upload").hasClass('disabled')) {
                return;
            }
            else {
                $("#wpci-config-upload").addClass('disabled');                
                $.ajax({
                    url: Wpci.config.ajaxUrl,
                    dataType: 'json',
                    method: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    success:function(response) {
                        $("#modal-settings-title").text('Success');                   
                        $("#modal-settings-body").text(response.data.msg);         
                        $("#modal-settings").modal('show');
                        $('#modal-settings').on('hidden.bs.modal', function () {
                            window.location.reload();
                        });
                    },
                    error:function() {
                        $("#modal-settings-title").text('Error');
                        $("#modal-settings-body").text('Error updating config');
                        $("#modal-settings").modal('show');
                    },
                    complete:function() {
                        $("#wpci-config-upload").removeClass('disabled');
                    }
                });                
            }      
        });        
        
        
        $("#wpci-settings-form").submit(function(e){
            e.preventDefault();
            $.ajax({
                url: Wpci.config.ajaxUrl,
                dataType: 'json',
                method: 'post',
                data: $(this).serialize(),
                success:function(response) {
                    $("#modal-settings-title").text('Success');                   
                    $("#modal-settings-body").text(response.data.msg);         
                    $("#modal-settings").modal('show');
                },
                error:function() {
                    $("#modal-settings-title").text('Error');
                    $("#modal-settings-body").text('Error saving setings');
                    $("#modal-settings").modal('show');
                },
                complete:function() {
                    $("#wpci-settings-submit").removeClass('disabled');
                }
            });
        });        
    });
}( jQuery ));