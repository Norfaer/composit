;(function($){
    function fetchPosts($container) {
        $.ajax({
            url:$container.data('action-url'),
            data:{
                p:$container.find('.post-type-select').val(),
                s:$container.find('.search-input').val(),                
            },
            dataType:'json',
            method:'post'
        });        
    }
    $(document).ready(function(){
        $('.postref-modal').each(function(){
            fetchPosts($(this));
        });
    });          
})(jQuery);

