;(function($){ 
    
    function reIndexMultiCollection($container) {
        var prefix = $container.data('name-template');
        $container.find('.sortable-item').each(function(index) {  
            $(this).data('index', index);
            $(this).find(':input:not(button):not([type="button"])').each(function(){
                if ($(this).data('name-template') || $(this).closest('.form-group').data('name-template')) {
                    $(this).prop('name', prefix + '[' + index + '][' + ($(this).data('name-template') || $(this).closest('.form-group').data('name-template'))+']');
                }
                else {
                    var oldName = $(this).prop('name');
                    var baseName = $(this).closest('.sortable-container').data('name-template');
                    var reg = new RegExp('^((?:' + baseName + ')|(?:qtranslate-fields\\[' + baseName + '\\]))' + '\\[(\\d+)\\](.*)$');
                    $(this).prop('name', oldName.replace(reg, '$1['+index+']$3'));
                }
            });
            $(this).find('.representation').html(getColectionItemHeader($(this), index));
        });        
    }
    
    function getColectionItemHeader($container, index) {        
        var str = $container.closest('.multi-collection').data('representation').replace(/%i/g, (index+1).toString());        
        $container.find(':input').each(function(index){
            str = str.replace(new RegExp('%'+(index+1),'g'), $(this).val());
        });
        return str;
    }
    
    $(document).on('click', '.multi-collection .sortable-controls', function(){
        var $item = $(this).closest('.sortable-item');
        $item.toggleClass('collapsed');       
    });
    
    $(document).on('click', '.multi-collection .add-collection', function(){
        var $parent = $(this).closest('.multi-collection'); 
        var $container = $parent.find('.sortable-container');
        //var $template = $parent.find('.multi-collection-template');        
        $container.data('count', parseInt($container.data('count'))+1);
        var increment = $container.data('count');
        var $clone = $parent.find('.multi-collection-template').clone(true).removeClass('multi-collection-template')        
        $clone.find(':input').each(function(){
            if (this.id) {
                this.id = this.id + '-' + increment;
            }
        });
        $clone.find('label').each(function(){
            $(this).attr('for', $(this).attr('for') + '-' + increment);
        });     
        $clone.find('[data-toggle="datetimepicker"]').each(function(){
            $(this).data('target', $(this).data('target') + '-' + increment);
        });     
        
        
     
        $clone.find('.input-mask').each(function() {
            $(this).inputmask($(this).data('pattern'), {
                placeholder: $(this).data('placeholder')
            })
        });        
        $clone.find('.color-picker').each(function(){
            $(this).wpColorPicker( {
                width: 400,
                palettes: true
            } );
        });
        $clone.appendTo($container);
        $clone.find('.date-picker').each(function() {
            $(this).datetimepicker({
                locale: Wpci.config.locale_short,
                format: $(this).data('date-format')
            });
        });
        
        $clone.find('.post-ref').each(function(){
            Vue.component('Postref', window.Postref.default);
            new Vue({el: this});            
        })
        
        reIndexMultiCollection($container);
        $container.sortable( "refresh" );
    });
    
    $(document).on('click', '.multi-collection .del-collection', function(){
        $(this).closest('.sortable-item').remove();
    });    
    
    $(document).on('change', '.multi-collection :input', function() {
        var $container = $(this).closest('.sortable-item');
        $container.find('.representation').html(getColectionItemHeader($container, $container.data('index')));
    });
    

    $('.multi-collection .sortable-container').each(function(){
        var $container = $(this);
        $container.data('count', $(this).find('.sortable-item').length)
        $(this).sortable({
            placeholder: "multicollection-placeholder",
            handle: ".sortable-item-header",
            //items: 'div.sortable-item',
            stop: function(){
                reIndexMultiCollection($container);
            }
        }); 
        reIndexMultiCollection($container);
    });
    
    
    $('.multi-collection-template').find(':input:not(button)').attr('name', '')


})(jQuery);

