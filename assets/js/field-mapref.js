loadScript('https://maps.googleapis.com/maps/api/js?key=' + Wpci.config.mapApiKey + '&language='+ Wpci.config.locale_short + '&libraries=geometry,places', InitAllMapContainers);

function SetMarkerInput(markers, input) {
    var str=''
    markers.forEach(function(item, index, arr){
        str = str + (index > 0 ? ';' : '') + item.getPosition().lat() + ',' + item.getPosition().lng();
    });
    input.value = str;
}


function InitMap(container) {
    var mapDiv = container.getElementsByClassName('map-container')[0];
    var dataSet = mapDiv.dataset;
    var hasSearch = !!dataSet.search;
    var slider = container.getElementsByClassName('input-zoom')[0];
    var centerX = container.getElementsByClassName('center-x')[0];
    var centerY = container.getElementsByClassName('center-y')[0];
    var path = container.getElementsByClassName('markers')[0];        
    var countryRestrict = { country: dataSet.country };
    var markers = [];
    var map = new google.maps.Map(mapDiv, {
        zoom: parseInt(dataSet.zoom),
        center: { lat: parseFloat(dataSet.centerY), lng: parseFloat(dataSet.centerX) },
        mapTypeId: 'terrain'
    });        
    var markersStr = path.value.split(';');
    if(hasSearch) {
        var searchInput = container.getElementsByClassName('input-search')[0];
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(searchInput);
        var autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */ (searchInput), {
              componentRestrictions: countryRestrict
            });
        autocomplete.addListener('place_changed', function(){
            var place = autocomplete.getPlace();
            if (place.geometry) {
                var marker = new google.maps.Marker({
                    position: place.geometry.location,
                    map: map
                });                
                marker.setMap(map);
                marker.addListener('rightclick', function() {
                    marker.setMap(null);
                    markers = markers.filter(function(e) { return e.map !==null; });
                    SetMarkerInput(markers, path);
                });
                markers.push(marker);  
                SetMarkerInput(markers, path);
                var bounds = new google.maps.LatLngBounds();
                markers.forEach(function(marker){
                    bounds.extend(marker.getPosition());                    
                });
                map.fitBounds(bounds);
            }            
        });
    }
    markersStr.forEach(function(item){
        var markerCoords = item.split(',');
        var marker;
        if (parseFloat(markerCoords[0]) && parseFloat(markerCoords[1])) {
            marker = new google.maps.Marker({
                position: { lat : parseFloat(markerCoords[0]), lng : parseFloat(markerCoords[1]) },
                map: map
            });       
            marker.setMap(map);
            marker.addListener('rightclick', function() {
                marker.setMap(null);
                markers = markers.filter(function(e) { return e.map !==null; });
                SetMarkerInput(markers, path);
            });
            markers.push(marker);                  
        }
    });
    map.addListener('zoom_changed', function(e) {
        slider.value = map.getZoom();
    });
    map.addListener('bounds_changed', function(e) {
        var center_lnlt = map.getCenter();
        centerX.value = center_lnlt.lng();
        centerY.value = center_lnlt.lat();
    });        
    map.addListener('rightclick', function(e){
        var marker = new google.maps.Marker({
            position: e.latLng,
            map: map
        });
        marker.addListener('rightclick', function() {
            marker.setMap(null);
            markers = markers.filter(function(e) { return e.map !==null; });
            SetMarkerInput(markers, path);
        });
        markers.push(marker);
        SetMarkerInput(markers, path);
    });

    slider.addEventListener('input', function( e ) {
        map.setZoom(parseInt(this.value));
    });
    //slider.noUiSlider.set(map.getZoom());
    centerX.addEventListener('change', function(e) {
        map.panTo( { lng:parseFloat(this.value), lat:parseFloat(centerY.value) } );
    })
    centerY.addEventListener('change', function(e) {
        map.panTo( { lng:parseFloat(centerX.value), lat:parseFloat(this.value) } );            
    });
}

function InitAllMapContainers() {
    var mapContainers = document.getElementsByClassName('mapref-container');    
    for(var i = 0; i < mapContainers.length; i++) {
        InitMap(mapContainers[i]);
    }
}