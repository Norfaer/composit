<?php
/*
Plugin Name: ComposIt
Description: Powerfull tool for creating sites
Version: 1.0
Author: Firsov Alexander
Text Domain: wpci
Domain Path: /languages
Author URI: https://bitbucket.org/Norfaer/
Plugin URI: https://bitbucket.org/Norfaer/
*/

use composit\core\Plugin;

if ( !defined('ABSPATH') ) {
	die('-1');
}

define('WPCI', plugin_basename(__FILE__));
define('WPCI_FILE', __FILE__);
define('WPCI_ROOT', plugin_dir_path(__FILE__));
define('WPCI_ROOT_URL', plugin_dir_url(__FILE__));
define('WPCI_INCLUDE', WPCI_ROOT . 'include' . DIRECTORY_SEPARATOR);
define('WPCI_VIEW', WPCI_ROOT . 'view' . DIRECTORY_SEPARATOR);
define('WPCI_ASSETS', WPCI_ROOT . 'assets'  . DIRECTORY_SEPARATOR);
define('WPCI_ASSETS_URL', WPCI_ROOT_URL . 'assets'  . DIRECTORY_SEPARATOR);
define('WPCI_IMAGE_CAHCE', ABSPATH . 'wp-content'. DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR);

require_once WPCI_INCLUDE . 'helpers.php';

add_action( 'plugins_loaded', function() {
    load_plugin_textdomain( 'wpci', false,  basename(dirname(__FILE__)).'/languages' );
});

$plugin = Plugin::getInstance();
$plugin->load();