<?php
namespace composit\modules;

use composit\core\Module;

class ModuleClone extends Module {
    
    public function clonePostLink( $actions, $post ) {
        if (current_user_can('edit_posts')) {
            $actions['clone'] = '<a href="' . wp_nonce_url('admin.php?action=clone_post_as_draft&post=' . $post->ID, basename(__FILE__), 'clone_nonce' ) . '" title="' . __('Duplicate this item','wpci') . '" rel="permalink">' . __('Clone','wpci') . '</a>';
        }
        return $actions;        
    }
    
    public function initModule() {}
    
    public function clonePostAsDraft() {
        global $wpdb;
        $reserved_meta = [
            '_edit_last',
            '_edit_lock',
        ];

        if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'clone_post_as_draft' == $_REQUEST['action'] ) ) ) {
                wp_die('No post to duplicate has been supplied!');
        }
        if ( !isset( $_GET['clone_nonce'] ) || !wp_verify_nonce( $_GET['clone_nonce'], basename( __FILE__ ) ) )
            return;
        $post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
        $post = get_post( $post_id );

        $current_user = wp_get_current_user();
        $new_post_author = $current_user->ID;

        if (isset( $post ) && $post != null) {
            $args = [
                'comment_status' => $post->comment_status,
                'ping_status'    => $post->ping_status,
                'post_author'    => $new_post_author,
                'post_content'   => $post->post_content,
                'post_excerpt'   => $post->post_excerpt,
                'post_name'      => $post->post_name,
                'post_parent'    => $post->post_parent,
                'post_password'  => $post->post_password,
                'post_status'    => 'draft',
                'post_title'     => $post->post_title,
                'post_type'      => $post->post_type,
                'to_ping'        => $post->to_ping,
                'menu_order'     => $post->menu_order
            ];

            $new_post_id = wp_insert_post( $args );

            $taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
            foreach ($taxonomies as $taxonomy) {
                $post_terms = wp_get_object_terms($post_id, $taxonomy, ['fields' => 'slugs']);
                wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
            }                

            $meta = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
            if (count($meta)!=0) {
                $index = 0;
                $sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) VALUES ";
                foreach ($meta as $meta_info) {
                    if( in_array($meta_info->meta_key, $reserved_meta) ) continue;
                    $sql_query.= $index ? ', ' : '';
                    $meta_key = $meta_info->meta_key;
                    $meta_value = addslashes($meta_info->meta_value);
                    $sql_query.= "($new_post_id, '$meta_key', '$meta_value')";
                    $index++;
                }
                if ($index) {
                    $wpdb->query($sql_query);
                }            
            }
            wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
            exit;
        } else {
            wp_die('Post creation failed, could not find original post: ' . $post_id);
        }        
    }
    
    
    public function frontHooks() {}
    
    public function adminHooks() {        
        add_action( 'admin_action_clone_post_as_draft', [ $this , 'clonePostAsDraft' ] );
        add_filter( 'post_row_actions', [ $this, 'clonePostLink' ], 10, 2 );
        add_filter( 'page_row_actions', [ $this, 'clonePostLink' ], 10, 2 );
    }
    
    public function commonHooks() {}
}