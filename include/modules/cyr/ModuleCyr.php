<?php

namespace composit\modules;

use composit\core\Module;

class ModuleCyr extends Module {
    
    public function adminHooks() {}

    public function commonHooks() {}

    public function frontHooks() {}

    public function initModule() {
	add_filter('sanitize_title', [ $this, 'latinize' ], 9);
	add_filter('sanitize_file_name', [ $this, 'latinizeFile' ],9);               
    }
    
    public function latinize($string) {
	$replace=[
		"'"=>"",
		"`"=>"",
		"а"=>"a","А"=>"a",
		"б"=>"b","Б"=>"b",
		"в"=>"v","В"=>"v",
		"г"=>"g","Г"=>"g",
		"д"=>"d","Д"=>"d",
		"е"=>"e","Е"=>"e",
		"ж"=>"zh","Ж"=>"zh",
		"з"=>"z","З"=>"z",
		"и"=>"i","И"=>"i",
		"й"=>"y","Й"=>"y",
		"к"=>"k","К"=>"k",
		"л"=>"l","Л"=>"l",
		"м"=>"m","М"=>"m",
		"н"=>"n","Н"=>"n",
		"о"=>"o","О"=>"o",
		"п"=>"p","П"=>"p",
		"р"=>"r","Р"=>"r",
		"с"=>"s","С"=>"s",
		"т"=>"t","Т"=>"t",
		"у"=>"u","У"=>"u",
		"ф"=>"f","Ф"=>"f",
		"х"=>"h","Х"=>"h",
		"ц"=>"c","Ц"=>"c",
		"ч"=>"ch","Ч"=>"ch",
		"ш"=>"sh","Ш"=>"sh",
		"щ"=>"sch","Щ"=>"sch",
		"ъ"=>"","Ъ"=>"",
		"ы"=>"y","Ы"=>"y",
		"ь"=>"","Ь"=>"",
		"э"=>"e","Э"=>"e",
		"ю"=>"yu","Ю"=>"yu",
		"я"=>"ya","Я"=>"ya",
		"і"=>"i","І"=>"i",
		"ї"=>"yi","Ї"=>"yi",
		"є"=>"ye","Є"=>"ye"
	];
	$result = iconv("UTF-8","UTF-8//IGNORE",strtr($string, $replace));
        $result = preg_replace('/[^-_0-9A-za-z]/', '-', $result);
        $result = preg_replace('/\-\-+/','-',$result);
        $result = trim($result,'-');
        return $result;        
    }
    
    public function latinizeFile($filename) {
        $fileparts = explode('.', $filename);
        $ext = array_pop($fileparts);
        $name = implode('.', $fileparts);        
        return $this->latinize($name). (empty($ext)?'':'.'.$ext);        
    }

}