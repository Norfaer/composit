<?php

namespace composit\modules;

use composit\core\Module;

class ModuleSeo extends Module {
    
    public $names = [ 'wpci-seo-title', 'wpci-seo-keywords', 'wpci-seo-description', 'wpci-seo-robots', 'wpci-og-title', 'wpci-og-image', 'wpci-og-description' ];
    
    public function adminHooks() {
        global $pagenow;
        add_action( 'add_meta_boxes', [$this, 'metaBoxes'], 10, 2 );
        if ('term.php' === $pagenow) {
            $taxonomy = $_REQUEST['taxonomy'];
            $t = get_taxonomy($taxonomy);            
            if ($t->public || $pt->publicly_queryable) {                
                add_action($taxonomy . '_edit_form', [$this ,'termEditFields'], 10, 2);
            }
        }
        add_action('save_post', [ $this, 'savePostMeta' ], 1, 3);        
        add_action('edit_term', [ $this,'saveTermMeta' ], 1, 3); 
        add_action('admin_post_save_seo_archive', [$this, 'saveArchiveSeo']);
    }

    public function commonHooks() {

    }

    public function frontHooks() {
        add_filter('pre_get_document_title', [$this, 'seoTitle']);
        add_action('wp_head', [$this, 'metaTags']);
    }

    public function initModule() {
        add_action('admin_menu', [ $this, 'archivesMenu' ]);        
    }
    
    public function archivesMenu() {
        $post_types = get_post_types([ 'public' => true, 'publicly_queryable'=>true ], 'objects', 'or');
        foreach($post_types as $post_type) {
            if ($post_type->has_archive || $post_type->_builtin) {
                add_submenu_page( "edit.php?post_type={$post_type->name}", 'Seo settings', 'Seo settings', 'manage_options', "archive-{$post_type->name}-seo-menu", [ $this, 'archiveSettingsPage']);
            }
        }
        add_submenu_page( "edit.php", 'Seo settings', 'Seo settings', 'manage_options', "archive-post-seo-menu", [ $this, 'archiveSettingsPage']);
    } 
    
    public function archiveSettingsPage() {
        $data = [];
        $post_type = isset($_REQUEST['post_type']) ? $_REQUEST['post_type'] : 'post';       
        $data['seo_title'] = get_option('wpci-seo-title-' . $post_type );
        $data['seo_keywords'] = get_option('wpci-seo-keywords-' . $post_type);
        $data['seo_description'] = get_option('wpci-seo-description-' . $post_type);
        $data['seo_robots'] = get_option('wpci-seo-robots-' . $post_type, 'index, follow');
        if (!$data['seo_robots']) {
            $data['seo_robots'] = 'index, follow';
        }
        $data['og_title'] = get_option('wpci-og-title-' . $post_type );
        $data['og_image'] = get_option('wpci-og-image-' . $post_type);
        $data['og_description'] = get_option('wpci-og-description-' . $post_type);
        $success = get_transient('wpci-seo-archive-'.$post_type.'-success');
        echo $this->view->render('archive', ['success' => $success, 'data' => $data, 'post_type' => $post_type]);
    }


    public function metaBoxes($post_type, $post) {
        $pt = get_post_type_object($post_type);
        if ($pt->public || $pt->publicly_queryable) {
            add_meta_box( 'wpci-seo-meta-box', __( 'SEO settings', 'wpci' ), [ $this, 'postMetaBox' ], $post_type, 'normal', 'default' );        
        }
    }
    
    public function postMetaBox($post, $cb) {
        $this->seoMetaData('post', $post->ID);
    }
    
    public function termEditFields($term, $taxonomy) {
        $this->seoMetaData('term', $term->term_id);
    }
        
    public function seoMetaData($meta_type, $id) {
        $data = [];
        $data['seo_title'] = get_metadata($meta_type, $id, 'wpci-seo-title', true);
        $data['seo_keywords'] = get_metadata($meta_type, $id, 'wpci-seo-keywords', true);
        $data['seo_description'] = get_metadata($meta_type, $id, 'wpci-seo-description', true);
        $data['seo_robots'] = get_metadata($meta_type, $id, 'wpci-seo-robots', true);
        if (!$data['seo_robots']) {
            $data['seo_robots'] = 'index, follow';
        }
        $data['og_title'] = get_metadata($meta_type, $id, 'wpci-og-title', true);
        $data['og_image'] = get_metadata($meta_type, $id, 'wpci-og-image', true);
        $data['og_description'] = get_metadata($meta_type, $id, 'wpci-og-description', true);
        echo $this->view->render('meta', ['data' => $data]);
    }    
    
    public function  savePostMeta($post_id, $post, $update) {
        if (!isset($_POST['wpci_save_seo']) ||
            !wp_verify_nonce($_POST['wpci_save_seo'], WPCI_ROOT_URL . '/save-seo') ||
            !current_user_can('edit_post', $post_id) ||
            wp_is_post_autosave($post_id) ||
            wp_is_post_revision($post_id))
        {
            return;
        }
        $names = [ 'wpci-seo-title', 'wpci-seo-keywords', 'wpci-seo-description', 'wpci-seo-robots' ];
        foreach($this->names as $name) {
            if (isset($_POST[$name])) {
                update_post_meta($post_id, $name, sanitize_text_field($_POST[$name]));
            }
        }
    }
    
    public function saveTermMeta( $term_id, $tt_id, $taxonomy) {
        if ( ! current_user_can('edit_term', $term_id) ) return;
        $names = [ 'wpci-seo-title', 'wpci-seo-keywords', 'wpci-seo-description', 'wpci-seo-robots' ];
        foreach($this->names as $name) {
            if (isset($_POST[$name])) {
                update_term_meta($term_id, $name, sanitize_text_field($_POST[$name]));
            }
        }
    }
    
    public function saveArchiveSeo() {        
        if (isset($_POST['post_type']) && isset($_POST['wpci_save_seo_arcive']) && wp_verify_nonce($_POST['wpci_save_seo_arcive'], WPCI_ROOT_URL . '/save-seo-archive')) {
            $post_type = $_POST['post_type'];
            $names = [ 'wpci-seo-title', 'wpci-seo-keywords', 'wpci-seo-description', 'wpci-seo-robots' ];
            foreach($this->names as $name) {
                if (isset($_POST[$name])) {
                    update_option($name . '-' . $post_type, sanitize_text_field($_POST[$name]), true);
                }
            }
            set_transient('wpci-seo-archive-'.$post_type.'-success', __('Seo settings updated successfully!'));
            wp_redirect(admin_url(($post_type === 'post' ? 'edit.php?' : "edit.php?post_type={$post_type}&") . "page=archive-{$post_type}-seo-menu"));
        }
        exit;        
    }

        public function seoTitle() {
        if (is_category() || is_tag() || is_tax()) {
            $term_id = get_queried_object_id();
            if (get_term_meta($term_id, 'wpci-seo-title', true)) {
                return get_term_meta($term_id, 'wpci-seo-title', true);
            }
        }
        else if (is_home() || is_singular()) {
            $post_id = get_queried_object_id();
            if (get_post_meta($post_id, 'wpci-seo-title', true)) {
                return get_post_meta($post_id, 'wpci-seo-title', true);
            }
        }
        else if (is_archive() && get_query_var('post_type')) {
            $post_type = get_query_var('post_type');
            return get_option('wpci-seo-title-' . $post_type );
        }
        return '';
    }
    
    private function metaTag($name, $content) {
        echo "<meta name=\"{$name}\" content=\"{$content}\" />\n";
    }
    
    public function metaTags() {
        if (is_category() || is_tag() || is_tax()) {
            $meta_type = 'term';
            $url = get_term_link(get_queried_object_id());
        }
        if (is_home() || is_singular()) {
            $meta_type = 'post';
            $url = get_permalink(get_queried_object_id());
        }        
        if (is_category() || is_tag() || is_tax() || is_home() || is_singular()) {
            $object_id = get_queried_object_id();
            $seo_keywords = get_metadata($meta_type, $object_id, 'wpci-seo-keywords', true);
            $seo_description = get_metadata($meta_type, $object_id, 'wpci-seo-description', true);
            $seo_robots = get_metadata($meta_type, $object_id, 'wpci-seo-robots', true); 
            $og_title = get_metadata($meta_type, $object_id, 'wpci-og-title', true);
            $og_image = get_metadata($meta_type, $object_id, 'wpci-og-image', true);
            $og_description = get_metadata($meta_type, $object_id, 'wpci-og-description', true);                        
        }
        if (is_archive() && get_query_var('post_type')) {
            $post_type = get_query_var('post_type');
            $url = get_post_type_archive_link($post_type);
            $seo_keywords = get_option('wpci-seo-keywords-' . $post_type);
            $seo_description = get_option('wpci-seo-description-' . $post_type);
            $seo_robots = get_option('wpci-seo-robots-' . $post_type, 'index, follow');    
            $og_title = get_option('wpci-og-title-' . $post_type );
            $og_image = get_option('wpci-og-image-' . $post_type);
            $og_description = get_option('wpci-og-description-' . $post_type);            
        }
        if ($seo_robots) {
            echo $this->view->tag('meta', [ 'name' => 'robots', 'content' => $seo_robots ]);
        }
        if ($seo_keywords) {
            echo $this->view->tag('meta', [ 'name' => 'keywords', 'content' => $seo_keywords ]);
        }
        if ($seo_description) {
            echo $this->view->tag('meta',[ 'name' => 'description', 'content' => $seo_description ]);
        }        
        if ($og_title) {
            echo $this->view->tag('meta',[ 'property' => 'og:title', 'content' => $og_title ]);            
        }
        if ($og_description) {
            echo $this->view->tag('meta',[ 'property' => 'og:description', 'content' => $og_description ]);            
        }
        if ($og_image) {
            echo $this->view->tag('meta',[ 'property' => 'og:image', 'content' => imageCacheResizeId((int)$og_image, 200, 200) ]);            
        }
        echo $this->view->tag('meta',[ 'property' => 'og:url', 'content' => $url ]);            
    }
}