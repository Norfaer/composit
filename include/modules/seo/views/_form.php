<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label for="seo-title"><?=__('Title Tag', 'wpci')?></label>
            <input id="seo-title" type="text" value="<?= htmlspecialchars($seo_title)?>" name="wpci-seo-title" class="form-control i18n-multilingual" aria-describedby="seo-title-description">
            <small id="seo-title-description" class="form-text text-muted"><?=__('This is the text you\'ll see at the top of your browser. Search engines view this text as the "title" of your page.', 'wpci')?></small>
        </div>
    </div>   
    <div class="col-sm-12">
        <div class="form-group">
            <label for="seo-keywords"><?=__('Meta Keywords Tag', 'wpci')?></label>
            <textarea id="seo-keywords" rows="2" name="wpci-seo-keywords" class="form-control i18n-multilingual" aria-describedby="seo-keywords-description"><?= htmlspecialchars($seo_keywords)?></textarea>
            <small id="seo-keywords-description" class="form-text text-muted"><?=__('A series of keywords you deem relevant to the page in question.', 'wpci')?></small>
        </div>
    </div>   
    <div class="col-sm-12">
        <div class="form-group">
            <label for="seo-description"><?=__('Meta Description Tag', 'wpci')?></label>
            <textarea id="seo-description" rows="2" name="wpci-seo-description" class="form-control i18n-multilingual" aria-describedby="seo-description-description"><?= htmlspecialchars($seo_description)?></textarea>
            <small id="seo-description-description" class="form-text text-muted"><?=__('A brief description of the page.', 'wpci')?></small>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="form-group">
            <label for="seo-robots"><?=__('Meta Robots Attribute', 'wpci')?></label>                
            <select id="seo-robots" aria-describedby="seo-robots-description" class="custom-select" name="wpci-seo-robots">
                <option value="index, follow" <?= selected($seo_robots, 'index, follow')?>>index, follow</option>
                <option value="noindex, follow" <?= selected($seo_robots, 'noindex, follow')?>>noindex, follow</option>
                <option value="index, nofollow" <?= selected($seo_robots, 'index, nofollow')?>>index, nofollow</option>
                <option value="noindex, nofollow" <?= selected($seo_robots, 'noindex, nofollow')?>>noindex, nofollow</option>
            </select>
            <small id="seo-robots-description" class="form-text text-muted"><?=__('An indication to search engine crawlers (robots or "bots") as to what they should do with the page.', 'wpci')?></small>
        </div>
    </div>
    <div class="col-sm-12">
        <h4><?=__('OpenGraph Tags', 'wpci')?></h4>
        <div class="row">
            <?php (new composit\core\Field(['value' => (int)$og_image,'name' => 'wpci-og-image','width' => 'col-sm-6', 'type' => 'image', 'multiple' => false, 'label' => 'og:image', 'params' => ['placeholder' => 'og:image']]))->render()?>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="og-title"><?=__('og:title Tag', 'wpci')?></label>
                    <input id="og-title" type="text" value="<?= htmlspecialchars($og_title)?>" name="wpci-og-title" class="form-control i18n-multilingual" aria-describedby="og-title-description">
                    <small id="og-title-description" class="form-text text-muted"><?=__('This is the text you\'ll see at the top of your browser. Search engines view this text as the "title" of your page.', 'wpci')?></small>
                </div>
                <div class="form-group">
                    <label for="og-description"><?=__('og:description Tag', 'wpci')?></label>
                    <textarea id="og-description" rows="2" name="wpci-og-description" class="form-control i18n-multilingual" aria-describedby="og-description-description"><?= htmlspecialchars($og_description)?></textarea>
                    <small id="og-description-description" class="form-text text-muted"><?=__('A brief description of the page.', 'wpci')?></small>
                </div>                
            </div>
        </div>
    </div>
</div>