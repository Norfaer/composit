<form action="<?= admin_url('admin-post.php')?>" method="post">    
    <?php wp_nonce_field( WPCI_ROOT_URL . '/save-seo-archive', 'wpci_save_seo_arcive' ) ?>
    <input type="hidden" value="<?=$post_type?>" name="post_type">
    <input type="hidden" value="save_seo_archive" name="action">
    <div class="bs-root">
        <div class="container">
        <h1><?=__('Archive SEO settings', 'wpci')?></h1>
        <?php if($success):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <strong><i class="fa-fa-check"></i>Success !</strong> <?=$success?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <?php delete_transient('wpci-seo-archive-'.$post_type.'-success'); endif; ?>        
          <?php echo $this->render('_form', $data) ?>
          <div class="row">
            <div class="col-sm-12">
              <button class="btn btn-info" role="button" type="submit"><i class="fa fa-save"></i> <?= __('Save settings', 'wpci')?></button>
            </div>
          </div>
        </div>
    </div>
</form>