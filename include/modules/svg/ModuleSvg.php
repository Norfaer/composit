<?php
namespace composit\modules;

use composit\core\Module;

class ModuleSvg extends Module {

    public function initModule() {
        
    }
   
    public function frontHooks() {}
    
    public function adminHooks() { 
        add_action( 'wp_ajax_svg_get_attachment_url', [ $this, 'get_attachment_url_media_library']);
        add_action( 'admin_enqueue_scripts', function() {
            wp_enqueue_style('svg-admin-css', $this->assets_url . '/css/admin-svg.css' );
            wp_register_script( 'svg-library', $this->assets_url . '/js/admin-svg.js', ['jquery'],  '1.0.0', true );
            wp_enqueue_script( 'svg-library' );
        } );
        add_filter('upload_mimes', function($mimes){
             $mimes['svg'] = 'image/svg+xml';
             return $mimes;
        });        
    }
    
    public function commonHooks() {}
    
    function get_attachment_url_media_library() {
        $url = '';
        $attachmentID = isset($_REQUEST['attachmentID']) ? $_REQUEST['attachmentID'] : '';
        if($attachmentID){
            $url = wp_get_attachment_url($attachmentID);
        }
        echo $url;
        die();    
    }    
    
}