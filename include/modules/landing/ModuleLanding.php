<?php
namespace composit\modules;

use composit\core\Module;

class ModuleLanding extends Module {
    
    public function initModule() {
        add_action('save_post_landing_page', [ $this, 'save' ], 1, 3);
        add_action('wp_ajax_landing_inline_save', [$this, 'saveInline']);
        add_action('wp_ajax_landing_import', [$this, 'importLanding']);
        if (is_admin()) {
            add_action('admin_menu',[$this, 'adminMenu']);
        }
    }
    
    public function frontHooks() {
        add_filter('single_template', [ $this, 'render']);
    }
    
    public function adminHooks() {
        add_action('load-post.php', [ $this, 'registerEditor' ], 1);
        add_action('load-options-reading.php', [$this, 'permaLink']);
        if (!empty($_REQUEST['post_type']) && $_REQUEST['post_type'] == 'landing_page') {
            add_action('load-post-new.php', [$this, 'registerEditor'], 1);
        }        
    }
    
    public function commonHooks() {
        register_post_type('landing_page', [
            'labels'             => [ 'name' => 'Лендинги', 'singular_name'=>'Лендинг' ],
            'description'        => __( 'Landings.', 'wpci' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => [ 'slug' => 'lp', 'with_front' => false ],
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => true,
            'menu_position'      => 5,
            'menu_icon'          => 'dashicons-media-spreadsheet',
            'supports'           => [ 'title' ]
        ]);
    }
    
    public function adminMenu() {
        add_submenu_page('edit.php?post_type=landing_page', 'Импорт лендинга из архива', 'Импорт', 'manage_options', 'landing_import', [$this, 'menuPage']);
    }
    
    public function menuPage() {
        $upload_dir = _wp_upload_dir();
        $can = is_writable($upload_dir['basedir']);
        wp_enqueue_script('import', $this->assets_url . '/js/import-landing.js', ['jquery'], '1.0.0', true);
?>
    <div class="wrap">
        <h1>Импорт лендинга из архива(.zip)</h1>
        <?php if($can): ?>
        <form method="post" action="<?= get_admin_url(null, 'admin-ajax.php')?>" id="import-form">            
            <input name="action" value="landing_import" type="hidden">
            <?php wp_nonce_field('import_landing')?>
            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row">Название лендинга </th>
                        <td>
                            <fieldset>
                                <legend class="screen-reader-text"><span>Название лендинга </span></legend>
                                <input name="landing_name" id="landing_name" value="" type="text" required>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Файл с архивом (.zip) </th>
                        <td>
                            <fieldset>
                                <legend class="screen-reader-text"><span>Файл с архивом (.zip)  </span></legend>
                                <input name="zip_file" type="file" id="zip_file" accept="application/zip,application/x-zip,application/x-zip-compressed">
                                <p class="description">Выберите файл с архивом. Максимально допустимый размер: <?=ini_get('upload_max_filesize')?></p>
                            </fieldset>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p class="submit">
                <span class="spinner" id="import-spinner" style="float:none;"></span>
                <input name="submit" id="submit" class="button button-primary" value="Импорт" type="submit">
            </p>
        </form>
        <?php else:?>
            <div id="setting-error-settings_updated" class="updated settings-error error is-dismissible"> 
                <p>Папка <strong><?=$upload_dir['basedir']?></strong> защищена от записи.</p>
                <button type="button" class="notice-dismiss"><span class="screen-reader-text">Скрыть это уведомление.</span></button>
            </div>        
        <?php endif; ?>
    </div>
<?php        
    }
    
    public function registerEditor() {
        add_action('add_meta_boxes_landing_page', [$this, 'registerMeta']);
    }
    
    public function registerMeta($post_type) {
        add_action('admin_enqueue_scripts', [$this, 'registerScriptsAdmin']);
        add_meta_box('wpci-landing_page-head', __('Head', 'wpci'), [ $this, 'metaBoxHead'], null, 'normal' , 'high');
        add_meta_box('wpci-landing_page-content', __('Content', 'wpci'), [ $this, 'metaBoxContent'], null, 'normal' , 'default');
    }
    
    public function metaBoxContent($post, $cb) {        
        $content = get_post_meta($post->ID, 'wpci_landing_inline_content', true);
        $settings = wp_enqueue_code_editor([ 'type'=>'text/html']);
        $params = safe_json($settings);        
?>                
    <div class="vg-app">
        <vg-editor value='<?=str_replace('\'', '&#39;', $content)?>' name="wpci_landing_inline_content" rows="80" :params='<?=$params?>'></vg-editor>    
    </div>
<?php        
    }

    public function metaBoxHead($post, $cb) {
        $meta = get_post_meta($post->ID, 'wpci_landing_head_meta', true);
        $css = get_post_meta($post->ID, 'wpci_landing_head_css', true);
        $js =  get_post_meta($post->ID, 'wpci_landing_head_js', true);
        $settings = wp_enqueue_code_editor([ 'type'=>'text/html']);
        $params = safe_json($settings);
?>
    <div class="vg-app">
        <vg-tabs>
            <vg-tab title="Meta" active="true">
                <vg-editor value='<?= str_replace('\'', '&#39;', $meta)?>' name="wpci_landing_head_meta" :params='<?=$params?>'></vg-editor>
            </vg-tab>
            <vg-tab title="Styles">
                <vg-editor value='<?= str_replace('\'', '&#39;', $css)?>' name="wpci_landing_head_css" :params='<?=$params?>'></vg-editor>
            </vg-tab>
            <vg-tab title="JavaScript">
                <vg-editor value='<?= str_replace('\'', '&#39;', $js)?>' name="wpci_landing_head_js" :params='<?=$params?>'></vg-editor>
            </vg-tab>
        </vg-tabs>
    </div>
<?php                
    }
    
    public function registerScriptsAdmin() {
        //wp_enqueue_script('wpci-ace', $this->url . '/assets/ace/ace.js', [], '5.3.0', true);
        //wp_enqueue_code_editor([ 'type'=>'text/html']);
        //wp_enqueue_script('wpci-landing-main', $this->url . '/assets/js/main.js', [], '5.3.0', true);
        
        $this->plugin->enqueueVue();
    }
    
    public function registerScriptFront() {
        
    }

    final public function save($post_id, $post, $update) {
        if (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }
        if (wp_is_post_revision($post) !== false || wp_is_post_autosave($post_id)) {
            return $post_id;
        }        
        $fields = [
            'wpci_landing_inline_content', 
            'wpci_landing_head_meta', 'wpci_landing_head_css', 'wpci_landing_head_js'
        ];
        foreach ($fields as $meta_key) {
            delete_post_meta($post_id, $meta_key);
            if (isset($_POST[$meta_key])) {
                $meta_value = $_POST[$meta_key];    
                if (is_array($meta_value)) {
                    $meta_value = array_filter($meta_value);
                    foreach($meta_value as $single) {
                        add_post_meta($post_id, $meta_key, $single, false);
                    }
                }
                else {
                    add_post_meta($post_id, $meta_key, $meta_value, true);
                }                         
            }
        }
    } 
    
    public function saveInline(){
        if ( !empty($_POST['id']) && current_user_can('edit_post',(int)$_POST['id'])) {
            update_post_meta((int)$_POST['id'], 'wpci_landing_inline_content', $_POST['content']);
            wp_send_json_success();
        }
        else {
            wp_send_json_error();
        }
    }    
    
    public function permaLink() {
        add_filter('wp_dropdown_pages', function($output, $args) {
            global $wpdb;
            $landings = $wpdb->get_results( "SELECT ID, post_title FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'landing_page'" );
            $select = '';
            foreach( $landings as $landing ) {
                $selected = $landing->ID == $args['selected'] ? ' selected' : '';
                $select .= "<option class=\"level-0\" value=\"{$landing->ID}\"{$selected}>{$landing->post_title} (Лэндинг)</option>";
            }
            return str_replace('</select>', $select . '</select>', $output);
        }, 2, 10);
    }
    
    
    public function importLanding() {
        if (!wp_doing_ajax()) {
            wp_die('You\'re doing it wrong!!!');
        }
        if ( !empty($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'], 'import_landing') ) {
            if (empty($_POST['landing_name']) || empty($_FILES['zip_file']) || $_FILES['zip_file']['error']!=0) {
                wp_send_json_error(['error'=>'Ошибка файла вложения']);            
            }
            $upload_dir = _wp_upload_dir();            
            $hash = pathinfo( $_FILES['zip_file']['name'] );
            $fn = $hash['filename'];
            $i = 1;
            while(file_exists($upload_dir['basedir'] . '/landings/'. $fn)) {
                $fn = $hash['filename'] . $i;
                $i++;
                if ($i > 999) {
                    wp_send_json_error(['error'=>'Ошибка при создании каталога '.$upload_dir['basedir'] . '/landings/'. $fn]);                                     
                }
            }
            $dir = $upload_dir['basedir'] . '/landings/'. $fn;             
            $url = $upload_dir['baseurl'] . '/landings/'. $fn;
            $re_dquotes = '/(?<q>"|\')(?!https?:\/\/)[.\/\\\\\\\\]*([^<>"\' ]+?)(\?[^"<>]*)?(?P=q)/s';
            $re_url = '/url\((?!https?:\/\/)[.\/\\\\\\\\]*([^<>\'" ]+?)(\?[^"<>]*)?\)/s';
            $re_body = '/<body[^>]*>(.*)<\/body>/is';            
            $re_meta = '/(<meta[^>]*>)|(<title[^>]*>(.*)<\/title>)/Usi';
            $re_script = '/<script[^>]*>(.*)<\/script>/Usi';
            $re_style = '/(<style[^>]*>(.*)<\/style>)|(<link[^>]*>)/Usi';
            mkdir($dir , 0777, true);
            move_uploaded_file($_FILES['zip_file']['tmp_name'], $dir.'/'.'index.zip');
            $zip = new \ZipArchive;    
            if ($zip->open($dir.'/'.'index.zip') === TRUE) {
                $files = [];
                for ($i = 0; $i < $zip->numFiles; $i++) {
                    $file =  $zip->getNameIndex($i);
                    if (substr($file, -1) !== '/') {
                        $files[] = $file;
                    }
                }
                $zip->extractTo($dir);
                $zip->close();                       
                unlink($dir.'/'.'index.zip');
                if (file_exists($dir . '/' . 'index.html')) {
                    $meta_db = [];
                    $style_db = [];
                    $script_db = [];
                    $html = file_get_contents($dir . '/' . 'index.html');
                    $html = preg_replace_callback($re_dquotes, function($matches) use ($files, $url) {
                        $key = array_search($matches[2], $files);
                        if ($key !== FALSE) {
                            return $matches['q'] . $url . '/' . $files[$key] . (empty($matches[3]) ? '' : $matches[3]) . $matches['q'] ;
                        }
                        return $matches[0];
                    }, $html);
                    $html = preg_replace_callback($re_url, function($matches) use ($files, $url) {
                        $key = array_search($matches[1], $files);
                        if ($key !== FALSE) {
                            return 'url(' . $url . '/' . $files[$key] . ')';
                        }
                        return $matches[0];
                    }, $html);
                    $html = preg_replace_callback($re_meta, function($matches) use (&$meta_db) {
                        $meta_db[]=$matches[0];
                        return '';
                    }, $html);
                    $html = preg_replace_callback($re_style, function($matches) use (&$style_db) {
                        $style_db[]=$matches[0];
                        return '';
                    }, $html);
                    $html = preg_replace_callback($re_script, function($matches) use (&$script_db) {
                        $script_db[]=$matches[0];
                        return '';
                    }, $html);
                    if (!preg_match($re_body, $html, $matches)) {
                        wp_send_json_error(['error'=>'Отсутствует тэг <body>']);      
                    }
                    $body = $matches[1];
                    $landing_id = wp_insert_post([
                        'post_content'   => '',
                        'post_date'      => date('Y-m-d H:i:s'),
                        'post_date_gmt'  => date('Y-m-d H:i:s'),
                        'post_excerpt'   => '',
                        'post_name'      => '',
                        'post_status'    => 'draft',
                        'post_title'     => $_POST['landing_name'],
                        'post_type'      => 'landing_page', 
                    ]);
                    add_post_meta($landing_id, 'wpci_landing_inline_content', $body, true);
                    add_post_meta($landing_id, 'wpci_landing_head_meta', implode("\n", $meta_db), true);
                    add_post_meta($landing_id, 'wpci_landing_head_css', implode("\n", $style_db), true);
                    add_post_meta($landing_id, 'wpci_landing_head_js', implode("\n", $script_db), true);
                    wp_send_json_success([ 'id' => $landing_id, 'files' => $files ]);            
                }
                else {
                    wp_send_json_error(['error'=>'В архиве отсутствет индексный файл index.html']);  
                }
            } else {
                wp_send_json_error(['error'=>'Ошибка обработки архива']);            
            }            
        }
        else {
            wp_send_json_error(['error'=>'Ошибка в параметрах запроса']);            
        }
    }
    
    public function render() {
        global $post;
        if  ($post->post_type === 'landing_page') {
            $content = get_post_meta($post->ID, 'wpci_landing_inline_content', true);
            $meta = special_apostrophe( get_post_meta($post->ID, 'wpci_landing_head_meta', true) );
            $css = special_apostrophe( get_post_meta($post->ID, 'wpci_landing_head_css', true) );
            $js = special_apostrophe( get_post_meta($post->ID, 'wpci_landing_head_js', true) );
            if (current_user_can('edit_post', $post->ID)) {      
                wp_enqueue_media();
                wp_enqueue_script('tinymce', $this->url .'/assets/tinymce/tinymce.min.js' );
                wp_enqueue_script('tinymce-jquery', $this->url .'/assets/tinymce/jquery.tinymce.min' );
                wp_enqueue_script('inline-edit', $this->url .'/assets/js/inline-edit.js' );
                wp_enqueue_style('inline-edit-style', $this->url .'/assets/css/inline-edit.css' );
            }
            else {
                remove_all_actions('wp_head');
                remove_all_actions('wp_footer');                
            }            
     
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); echo $meta; echo $css;?>        
</head>
<body <?php body_class(); ?>>    
    <?php if (current_user_can('edit_post', $post->ID)) :?>    
        <script>ajax_url="<?= get_admin_url(null, 'admin-ajax.php')?>"</script>
        <div id="landing-editor-toolbar" style="transition: bottom 2s;position: fixed;z-index:65533;bottom:0"></div>
        <div id="landing-editor-content" data-post-id="<?=$post->ID?>"><?=$content?></div>
    <?php else: ?>
        <?=$content?>
    <?php endif; ?>
<?php wp_footer(); echo $js; ?>       
</body>
</html>
<?php
            exit;
        }
    }    
}