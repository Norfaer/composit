var $loading = false;
;(function($){
    $('#import-form').submit(function(e){        
        e.preventDefault();
        if (!$loading) {
            $.ajax({
                url:$(this).attr('action'),
                method:'POST',
                dataType:'json',
                data: new FormData(this),
                cache: false,
                contentType: false,
                processData: false,    
                beforeSend: function(){
                    $loading=true;
                    $("#import-form input").prop( "disabled", true );
                    $("#import-spinner").css('visibility', 'visible');
                },
                success: function(response) {
                    if (response.success) {
                        alert('Successfully imported');
                    }
                    else {
                        alert('Error: ' + response.data.error);
                    }
                },
                complete: function() {                
                    $("#import-form input").prop( "disabled", false );
                    $("#import-spinner").css('visibility', 'hidden');
                    $loading=false;
                }
            });
        }
    });
})(jQuery);