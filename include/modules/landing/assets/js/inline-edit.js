var image_frame;
var image_url;
var image_elem;
var image_callback;

;(function($){
    var saveHandler = function(e, e2) {
        var $box = e;
        var $data = {
            action:'landing_inline_save',
            id:$box.bodyElement.dataset.postId,
            content:$box.getContent()
        };
        $.ajax({
            url:ajax_url,
            data: $data,
            method:'POST',
            success: function(data) {
                if (data.success) {
                    $box.notificationManager.open({
                        text: 'Страница сохранена!',
                        type: 'success',
                        timeout: 3000
                    });
                }
                else {
                    $box.notificationManager.open({
                        text: 'Ошибка при сохранении!',
                        type: 'error',
                        timeout: 5000
                    });
                }
            },
            error : function() {
                $box.notificationManager.open({
                    text: 'Неверный ответ сервера!',
                    type: 'error',
                    timeout: 5000
                });
            }
        });
    };    

    var imageHandler = function(cb, value, meta) {
        image_callback = cb;
        if(image_frame){
            image_frame.open();
            return;
        }
        image_frame = wp.media({
            title: 'Выберите изображение',
            multiple : false,
            library : {
                type : 'image',
            },
        });
        image_frame.on('select',function() {
            var selection =  image_frame.state().get('selection');
            selection.each(function(attachment) {
                image_url = attachment.attributes.url;
                image_callback(image_url,{});
            });
        });
        image_frame.on('open',function() {
        });
        image_frame.open();
    };
    
    $(document).ready(function(){
        var toolbar, plugins, menu;
        $(this).addClass('inline-edit-box');
        plugins = 'save visualblocks visualchars image link media template code codesample table lists textcolor colorpicker help';
        toolbar = 'formatselect | bold italic strikethrough forecolor backcolor | link image | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | code removeformat | save cancel';
        menu = 'edit insert view format table tools help';
        tinymce.init({
            selector: '#landing-editor-content',
            inline: true,
            schema: 'html5',
            menubar: menu,
            plugins: plugins,
            toolbar: toolbar,
            lineheight_formats: "18pt 20pt 22pt 24pt 26pt 36pt 40pt 46pt 50pt",
            file_picker_types: 'image',
            forced_root_block : '',
            force_p_newlines : false,
            content_css: [],
            fixed_toolbar_container: '#landing-editor-toolbar',
            file_picker_callback: imageHandler,
            save_onsavecallback:saveHandler
        });
        $(window).scroll(function (event) {
            var scroll = $(window).scrollTop();
            if (scroll > 100) {
                $('#landing-editor-toolbar').css('bottom','auto');
            }
            else {
                $('#landing-editor-toolbar').css('bottom','0px');
            }
            // Do something
        });                        
    });

})(jQuery);