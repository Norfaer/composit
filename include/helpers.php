<?php


spl_autoload_register(function($class){
    if (strpos($class, 'composit') === 0) {
        require_once WPCI_INCLUDE . str_replace( '\\', DIRECTORY_SEPARATOR, substr($class, 8) ) . '.php';
    }
});

function safe_json($mxed) {
    return str_replace( '\'', '&#39;', json_encode($mxed) );
}
function special_apostrophe($str) {
    return str_replace( '&#39;', '\'', $str );
}


function imageResize($path_from, $path_to, $max_width, $max_height) {
    if (extension_loaded('imagick')) {
        imageResizeImagick($path_from, $path_to, $max_width, $max_height);
    }
    else {
        imageResizeGd($path_from, $path_to, $max_width, $max_height);
    }    
}


function imageResizeGd($path_from, $path_to, $max_width, $max_height) {
    list($source_width, $source_height, $source_type) = getimagesize($path_from);
    switch ($source_type) {
        case IMAGETYPE_GIF:
            $source_gdim = imagecreatefromgif($path_from);
            break;
        case IMAGETYPE_JPEG:
            $source_gdim = imagecreatefromjpeg($path_from);
            break;
        case IMAGETYPE_PNG:
            $source_gdim = imagecreatefrompng($path_from);
            break;
    }
    $source_aspect_ratio = $source_width / $source_height;
    $desired_aspect_ratio = $max_width / $max_height;
    if ($source_aspect_ratio > $desired_aspect_ratio) {
        $temp_height = $max_height;
        $temp_width = ( int ) ($max_height * $source_aspect_ratio);
    } else {
        $temp_width = $max_width;
        $temp_height = ( int ) ($max_width / $source_aspect_ratio);
    }
    $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
    imagecopyresampled( $temp_gdim,	$source_gdim, 0, 0, 0, 0, $temp_width, $temp_height, $source_width, $source_height );
    $x0 = ($temp_width - $max_width) / 2;
    $y0 = ($temp_height - $max_height) / 2;
    $desired_gdim = imagecreatetruecolor($max_width, $max_height);
    imagecopy( $desired_gdim, $temp_gdim, 0, 0, $x0, $y0, $max_width, $max_height );
    switch ($source_type) {
        case IMAGETYPE_GIF:
            imagegif($desired_gdim, $path_to);
            break;
        case IMAGETYPE_JPEG:
            imagejpeg($desired_gdim, $path_to);
            break;
        case IMAGETYPE_PNG:
            imagepng($desired_gdim, $path_to);
            break;
    }			
}


function imageResizeImagick($path_from, $path_to, $max_width, $max_height) {
    $image = new \Imagick($path_from);        
    switch ($image->getimageformat()) {
        case 'GIF':
            $image = $image->coalesceimages();
            foreach($image as $frame) {
                $frame->cropthumbnailimage($max_width, $max_height);
                $frame->setimagepage($max_width, $max_height, 0, 0);
            }
            $image = $image->deconstructImages();
            $image->writeimages($path_to, true);
            break;
        default:                 
            $image->setImageCompressionQuality(80);
            $image->stripImage(); 			
            $image->cropthumbnailimage($max_width, $max_height);
            $image->writeimage($path_to);
            break;
    }
    $image->clear();  
    $image->destroy();
}

function imageCacheResizeUrl($url, $max_width, $max_height) {
    try {
        $url_parts = explode('?', $url);
        $cache_path = $max_width . 'x' . $max_height . '/' . str_replace(['https://', 'http://'], ['', ''], $url_parts[0]);
        if ( file_exists( WPCI_IMAGE_CAHCE . $cache_path ) ) {
            return content_url('cache/' . $cache_path);
        }
        $dir = pathinfo(WPCI_IMAGE_CAHCE . $cache_path, PATHINFO_DIRNAME);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        imageResize($url, WPCI_IMAGE_CAHCE . $cache_path, $max_width, $max_height);                     
        return content_url('cache/' . $cache_path);
    }
    catch (\ImagickException $e) {
        return $url;
    }
}

function imageCacheResizeLocal($path, $max_width, $max_height) {
    try {
        $cache_path = $max_width . 'x' . $max_height . '/' . str_replace( ABSPATH, '', $path);
        if ( file_exists( WPCI_IMAGE_CAHCE . $cache_path ) ) {
            return content_url('cache/' . $cache_path);
        }
        $dir = pathinfo(WPCI_IMAGE_CAHCE . $cache_path, PATHINFO_DIRNAME);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        imageResize($path, WPCI_IMAGE_CAHCE . $cache_path, $max_width, $max_height);                     
        return content_url('cache/' . $cache_path);
    }
    catch (\ImagickException $e) {
        return $path;
    }    
}

function imageCacheResizeId($image_id, $max_width, $max_height) {
    $path = get_attached_file((int)$image_id);
    if (!$path) {
        $path = WPCI_ASSETS . '/img/no-image.png';
    }
    return imageCacheResizeLocal($path, $max_width, $max_height);
}

function radio_meta_box($post, $params) {
        $taxonomy = get_taxonomy($params['args']['taxonomy']);
        $terms = get_terms( [ 'hide_empty' => 0, 'taxonomy' => $taxonomy->name ] );
        $postterms = get_the_terms( $post, $taxonomy->name );
        if($postterms === false || $postterms instanceof WP_Error)
        { $postterms = []; }        
        ?>
        <div id="taxonomy-<?=$params['id']?>" class="categorydiv">
            <div id="<?=$taxonomy->name?>-all" class="tabs-panel">
                <ul id="<?=$taxonomy->name?>-checklist" class="categorychecklist form-no-clear">
                     <?php $index=0; foreach($terms as $term): ?>
                        <li id="<?=$taxonomy->name?>-<?=$term->term_id?>">
                            <label class="selectit">
                                <?php if($taxonomy->hierarchical):?>
                                <input type="radio" id="in-<?=$taxonomy->name?>-<?=$term->term_id?>" name="tax_input[<?=$taxonomy->name?>]" <?= checked(in_array($term, $postterms) || (empty($postterms) && $index === 0))?> value="<?=$term->term_id?>"/><?=$term->name?><br />
                                <?php else: ?>
                                <input type="radio" id="in-<?=$taxonomy->name?>-<?=$term->term_id?>" name="tax_input[<?=$taxonomy->name?>]" <?= checked(in_array($term, $postterms) || (empty($postterms) && $index === 0))?> value="<?=$term->name?>"/><?=$term->name?><br />
                                <?php endif; ?>
                            </label>
                        </li>
                    <?php $index++; endforeach; ?>
                </ul>
            </div>
        </div>    
    <?php
}