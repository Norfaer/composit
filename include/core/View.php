<?php

namespace composit\core;

/**
 * Core base class to render template files.
 *
 * In order to render template, call ->render() method;
 *
 * @since 1.0.0
 */
class View {
    use Base;
    
    public $path = [];
    
    private $full_path;
        
    public function init() {
        if (!$this->path) {
            $this->path[] = WPCI_VIEW;
        }
    }
    
    private function locate_view( $view ) {
        foreach($this->path as $path) {
            if (file_exists( $path . $view . '.php' )) {
                return $path . $view . '.php';
            }
        }
        return false;
    }
    
    public function render($template = '', $data = []) {
        $this->full_path = $this->locate_view($template);
        if ($this->full_path) {
            ob_start();
            extract($data);
            require $this->full_path;
            return ob_get_clean();
        }
        return false;        
    }
    
    public function tag($tag, $attr = [], $content = '') {
        $void = [ 'area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 'input', 'keygen', 'link', 'meta', 'param', 'source', 'track', 'wbr' ];
        $tag = strtolower($tag);        
        $output = '<' . $tag ;
        foreach($attr as $key => $val) {
            $output .= ' ' . $key . '="' .  htmlspecialchars($val) . '"';
        }
        $output .= in_array($tag, $void) ? '/>' : '>'.$content.'</'. $tag . '>';
        return $output;
    }
    
}