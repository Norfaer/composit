<?php

namespace composit\core;

class Plugin {
    use BaseSingleton;
    
    private $config = [];
    private $options = [];
    private $posts = [];
    private $taxonomies = [];
    
    private $default_options = [
        'gapi_key' => '',
    ];
    
    public function init() {
        $this->config = array_merge_recursive([
            'posts' => [ 'post' => ['slug'=>'post'], 'page' => ['slug'=>'page'] ],
            'taxonomies' => [],
            'postMeta' => [],
            'taxonomyMeta' => [],
            'modules' => [],
            'templates' => []
        ], json_decode(get_option('__wpci_config', '{}'), true));
        $this->options = get_option('__wpci_options', $this->default_options);
        if (is_admin()) {
            register_activation_hook(WPCI_FILE, [ $this, 'activate' ]);
            register_deactivation_hook(WPCI_FILE, [ $this, 'deactivate' ]);
            add_filter( 'plugin_action_links_' . WPCI, [ $this, 'pluginActions' ] );   
            add_action( 'admin_post_wpci_update_config', [ $this, 'updateConfig' ] );
            add_action( 'admin_menu', [ $this, 'adminMenu' ] );
            add_action( 'admin_enqueue_scripts', [$this, 'registerScriptsAdmin'], 999);
//            add_action( 'plugins_loaded', [ $this, 'loadTextDomain' ]);
            Ajax::getInstance();
        }
    }
    
    public function load() {
        foreach($this->config['posts'] as $post_type => $post_config) {
            $post_config['meta'] = [];
            foreach($this->config['postMeta'] as $meta_name => $meta_params) {
                if (empty($meta_params['restrict_by_type']) || (is_array($meta_params['restrict_by_type']) ?  in_array($post_type, $meta_params['restrict_by_type']) : $post_type ==  $meta_params['restrict_by_type']))            
                    $post_config['meta'][$meta_name] = $meta_params;
            }            
            $this->posts[$post_type] =  new Post($post_config);
        }
        foreach($this->config['taxonomies'] as $taxonomy => $tax_config) {
            $tax_config['meta'] = [];
            foreach($this->config['taxonomyMeta'] as $meta_name => $meta_params) {
                if (empty($meta_params['restrict_by_taxonomy']) || (is_array($meta_params['restrict_by_taxonomy']) ?  in_array($taxonomy, $meta_params['restrict_by_taxonomy']) : $taxonomy ==  $meta_params['restrict_by_taxonomy']))            
                    $tax_config['meta'][$meta_name] = $meta_params;
            }
            $this->taxonomies[$taxonomy] =  new Taxonomy($tax_config);
        }        
        foreach ($this->config['modules'] as $module) {
            if (file_exists($file =  WPCI_INCLUDE . "modules/{$module['name']}/{$module['class']}.php")) {
                require_once $file;
                $class = "composit\\modules\\{$module['class']}"  ;
                if (class_exists($class) && in_array('composit\core\Module', class_parents($class))) {         
                    $class::getInstance([ 'name' => $module['name'], 'params' => empty($module['params']) ? [] :  $module['params'] ] );
                }
            }
        }
    }
    
    public function loadTextDomain() {
        if (!load_plugin_textdomain( 'wpci', false,  WPCI_ROOT ))
                die('error '.WPCI_ROOT . 'languages');
    }

    public function activate() {
        $json = WPCI_ROOT  . 'config.json';
        if (file_exists($json)) {
            $config  = json_decode(file_get_contents($json), true);
            if (!$config || !$this->validateConfig($config)) {
                die(__('Error! Config is invalid'));
            }
            update_option('__wpci_config', json_encode($config), true);
        }
    }
    
    public function deactivate() {
        
    }
    
    public function updateConfig() {
        if (!current_user_can('activate_plugins')) {
            wp_die(__('You\'re not allowed to manage plugins', 'wpci'), 'Error');            
        }
    }
    
    public function pluginActions($links) {
        $links[] = '<a href="'. esc_url( get_admin_url(null, 'admin.php?page=wpci') ) .'">' . __('Settings','wpci') . '</a>';
        return $links;
    }
    
    public function adminMenu() {
        add_menu_page(__('Wpci plug-in Settings','wpci'), 'ComposIt', 'manage_options', 'wpci', [$this, 'renderAdminView'],'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB2aWV3Qm94PSIwIDAgNzIyLjE0MjgyIDYxOC4wNDIyNCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48dGl0bGU+QVBJIEljb248L3RpdGxlPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0zMDYuMjYzNTEsMTkwLjg0NDk2KSI+PGcgZmlsbD0iIzdmMDBmZiI+PGcgZmlsbD0iIzAwMCIgdHJhbnNmb3JtPSJtYXRyaXgoNS4zNDkyMSwwLDAsNS4zNDkyMSwtMTMzMi4wMDM1LC0xODU3Ljk2OTUpIj48cGF0aCBkPSJtLTE4MC45NzUzLDUzNS43MDc1Mmg3My45NDM4M3Y3My45NDM4MmgtNzMuOTQzODN6IiBmaWxsPSIjMDAwIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIHRyYW5zZm9ybT0ibWF0cml4KC44NzI3NCwtLjQ4ODE5LC44NzI3NCwuNDg4MTksMCwwKSIvPjxwYXRoIGQ9Im0tNjM3LjI3MjE2LDQzMS4xMzA3NGgzNi4zNTk5NnY3NC40NzgzOWgtMzYuMzU5OTZ6IiBmaWxsPSIjMDAwIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEsLjg3Mjc0LC0uNDg4MTksMCwwKSIvPjxwYXRoIGQ9Im0tMjE5LjUxOTY1LTQyNS40MDE2N2gzNi4zNTk5NnY3NC40NzgzOWgtMzYuMzU5OTZ6IiBmaWxsPSIjMDAwIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEsLS44NzI3NCwtLjQ4ODE5LDAsMCkiLz48cGF0aCBkPSJtNDAuNzE0MjktMTA2LjQyNzU3YzAsOC4wODcwMy02LjU1NTgzLDE0LjY0Mjg2LTE0LjY0Mjg2LDE0LjY0Mjg2cy0xNC42NDI4Ni02LjU1NTgzLTE0LjY0Mjg2LTE0LjY0Mjg2IDYuNTU1ODMtMTQuNjQyODUgMTQuNjQyODYtMTQuNjQyODUgMTQuNjQyODYsNi41NTU4MyAxNC42NDI4NiwxNC42NDI4NXoiIGZpbGw9IiNmZmYiIGZpbGwtcnVsZT0iZXZlbm9kZCIgdHJhbnNmb3JtPSJtYXRyaXgoMS4yNDM5LDAsMCwtLjY5NTgyLDMwNS45NDAzMiwyNzMuNzE5MjYpIi8+PHBhdGggZD0ibTQwLjcxNDI5LTEwNi40Mjc1N2MwLDguMDg3MDMtNi41NTU4MywxNC42NDI4Ni0xNC42NDI4NiwxNC42NDI4NnMtMTQuNjQyODYtNi41NTU4My0xNC42NDI4Ni0xNC42NDI4NiA2LjU1NTgzLTE0LjY0Mjg1IDE0LjY0Mjg2LTE0LjY0Mjg1IDE0LjY0Mjg2LDYuNTU1ODMgMTQuNjQyODYsMTQuNjQyODV6IiBmaWxsPSIjMDAwIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIHRyYW5zZm9ybT0ibWF0cml4KDEuMjQzOSwwLDAsLS42OTU4MiwzMDUuOTQwMzIsMjY2LjQwMTIxKSIvPjxwYXRoIGQ9Im00MC43MTQyOS0xMDYuNDI3NTdhMTQuNjQyODYsMTQuNjQyODYgMCAxLDEgLTI5LjI4NTcyLDAgMTQuNjQyODYsMTQuNjQyODYgMCAxLDEgMjkuMjg1NzIsMHoiIGZpbGw9IiNmZmYiIGZpbGwtcnVsZT0iZXZlbm9kZCIgdHJhbnNmb3JtPSJtYXRyaXgoMS4yNDM5LDAsMCwtLjY5NTgyLDMzOS45MDQ2LDI5MS40NzA2OSkiLz48cGF0aCBkPSJtNDAuNzE0MjktMTA2LjQyNzU3YTE0LjY0Mjg2LDE0LjY0Mjg2IDAgMSwxIC0yOS4yODU3MiwwIDE0LjY0Mjg2LDE0LjY0Mjg2IDAgMSwxIDI5LjI4NTcyLDB6IiBmaWxsPSIjMDAwIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIHRyYW5zZm9ybT0ibWF0cml4KDEuMjQzOSwwLDAsLS42OTU4MiwzMzkuOTA0NiwyODQuMTUyNjQpIi8+PHBhdGggZD0ibTQwLjcxNDI5LTEwNi40Mjc1N2MwLDguMDg3MDMtNi41NTU4MywxNC42NDI4Ni0xNC42NDI4NiwxNC42NDI4NnMtMTQuNjQyODYtNi41NTU4My0xNC42NDI4Ni0xNC42NDI4NiA2LjU1NTgzLTE0LjY0Mjg1IDE0LjY0Mjg2LTE0LjY0Mjg1IDE0LjY0Mjg2LDYuNTU1ODMgMTQuNjQyODYsMTQuNjQyODV6IiBmaWxsPSIjZmZmIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIHRyYW5zZm9ybT0ibWF0cml4KDEuMjQzOSwwLDAsLS42OTU4MiwzNzQuOTA0NjEsMjczLjcxOTI1KSIvPjxwYXRoIGQ9Im00MC43MTQyOS0xMDYuNDI3NTdjMCw4LjA4NzAzLTYuNTU1ODMsMTQuNjQyODYtMTQuNjQyODYsMTQuNjQyODZzLTE0LjY0Mjg2LTYuNTU1ODMtMTQuNjQyODYtMTQuNjQyODYgNi41NTU4My0xNC42NDI4NSAxNC42NDI4Ni0xNC42NDI4NSAxNC42NDI4Niw2LjU1NTgzIDE0LjY0Mjg2LDE0LjY0Mjg1eiIgZmlsbD0iIzAwMCIgZmlsbC1ydWxlPSJldmVub2RkIiB0cmFuc2Zvcm09Im1hdHJpeCgxLjI0MzksMCwwLC0uNjk1ODIsMzc0LjkwNDYxLDI2Ni40MDEyMSkiLz48cGF0aCBkPSJtNDAuNzE0MjktMTA2LjQyNzU3YTE0LjY0Mjg2LDE0LjY0Mjg2IDAgMSwxIC0yOS4yODU3MiwwIDE0LjY0Mjg2LDE0LjY0Mjg2IDAgMSwxIDI5LjI4NTcyLDB6IiBmaWxsPSIjZmZmIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIHRyYW5zZm9ybT0ibWF0cml4KDEuMjQzOSwwLDAsLS42OTU4MiwzMzkuOTA0NiwyNTUuMTEwNzIpIi8+PHBhdGggZD0ibTQwLjcxNDI5LTEwNi40Mjc1N2ExNC42NDI4NiwxNC42NDI4NiAwIDEsMSAtMjkuMjg1NzIsMCAxNC42NDI4NiwxNC42NDI4NiAwIDEsMSAyOS4yODU3MiwweiIgZmlsbD0iIzAwMCIgZmlsbC1ydWxlPSJldmVub2RkIiB0cmFuc2Zvcm09Im1hdHJpeCgxLjI0MzksMCwwLC0uNjk1ODIsMzM5LjkwNDYsMjQ3Ljc5MjY4KSIvPjwvZz48L2c+PC9nPjwvc3ZnPg==');
    }
    
    public function renderAdminView() {
        wp_enqueue_script('wpci-settings');
        $data['ajax_url'] = get_admin_url(null, 'admin-ajax.php');
        $data['options'] = $this->options;
        echo (new View())->render('settings', $data);
    }
    
    public function registerScriptsAdmin() {
        wp_register_script( 'wpci-vue-build', WPCI_ASSETS_URL . '/js/vue-build.js', [ ], '1.0.0', true);
        wp_register_script( 'wpci-field-image', WPCI_ASSETS_URL . 'js/field-image.js', [ 'jquery' ], '1.0.0', true );
        wp_register_script( 'wpci-moment', WPCI_ASSETS_URL . 'bootstrap/js/plugins/moment.min.js', [ 'wpci-bootstrap' ], '1.0.0', true);        
        wp_register_script( 'wpci-mask', WPCI_ASSETS_URL . 'bootstrap/js/plugins/input-mask.min.js', [ 'jquery' ], '4.0.0', true);        
        wp_register_script( 'wpci-field-datepicker', WPCI_ASSETS_URL . 'bootstrap/js/plugins/tempusdominus-bootstrap-4.min.js', [ 'jquery', 'wpci-moment' ], '1.0.0', true);
        wp_register_script( 'wpci-field-postref', WPCI_ASSETS_URL . 'js/postref.min.js', [ 'jquery', 'wpci-vue-build' ], '1.0.0', true);
        wp_register_script( 'wpci-field-mapref', WPCI_ASSETS_URL . 'js/field-mapref.js', [ ], '1.0.0', true);
        wp_register_script( 'wpci-field-multicollection', WPCI_ASSETS_URL . 'js/field-multicollection.js', [ 'jquery' ], '1.0.0', true);
        wp_register_script( 'wpci-settings', WPCI_ASSETS_URL . 'js/wpci-settings.js', [ 'jquery' ], '1.0.0', true);
        wp_register_script( 'wpci-common', WPCI_ASSETS_URL . 'js/wpci-common.js', [ 'jquery' ], '1.0.0', true);
        wp_localize_script( 'wpci-common', 'Wpci', [ 
            'config' => [
                'ajaxUrl' => admin_url('admin-ajax.php'),
                'imageEditUrl' => admin_url('post.php?action=edit&image-editor&post='),
                'mapApiKey' => $this->options['gapi_key'],
                'locale' => get_locale(),
                'locale_short' => substr(get_locale(), 0, 2)
            ],
            'messages' => [
                'errRequired' => __('This field is required', 'wpci'),
                'errEmail' => __('Provide valid email', 'wpci'),
                'errPhone' => __('Provide valid phone number', 'wpci'),
                'errUrl' => __('Provide valid url', 'wpci'),
                'errNumber' => __('Provide valid number', 'wpci'),
                'errPattern' => __('Must match pattern &quot;{0}&quot;', 'wpci'),
            ]            
        ]);
        wp_enqueue_style('wpci-font-awesome', WPCI_ASSETS_URL . 'bootstrap/css/font-awesome.min.css');
        wp_enqueue_style('wpci-bootstrap-style', WPCI_ASSETS_URL . 'bootstrap/css/bootstrap.min.css');        
        wp_enqueue_style('wpci-postref-style', WPCI_ASSETS_URL . 'bootstrap/css/postref.min.css');        
        wp_enqueue_style('wpci-style', WPCI_ASSETS_URL . 'bootstrap/css/wpci.css');        
        wp_enqueue_style('wpci-datepicker-style', WPCI_ASSETS_URL . 'bootstrap/css/tempusdominus-bootstrap-4.min.css');        
        wp_enqueue_script('wpci-bootstrap', WPCI_ASSETS_URL . 'bootstrap/js/core/bootstrap.bundle.min.js', [ 'jquery' ], '4.0.0', true);
        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_script('wpci-common');
    }

    public function validateConfig(&$config) {     
        return true;
    }
        
}