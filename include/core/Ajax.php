<?php
namespace composit\core;

class Ajax {
    use BaseSingleton;    
    
    public function init() {
        add_action( 'wp_ajax_get_posts', [ $this, 'actionGetPosts' ]);
        add_action( 'wp_ajax_wpci_save_settings', [  $this, 'actionSaveSettings' ]);
        add_action( 'wp_ajax_wpci_update_config', [  $this, 'actionUpdateConfig' ]);
        add_action( 'wp_ajax_wpci_upload_config', [  $this, 'actionUploadConfig' ]);
    }
    
    public function actionGetPosts() {
        global $wpdb;           
        $page = isset($_POST['page']) ? (int)$_POST['page'] : 1;
        $search = isset($_POST['s']) ? '%' . $wpdb->esc_like($_POST['s']) . '%' : '%%';
        $post_type = isset($_POST['p']) ? $_POST['p'] : 'post';
        $offset = ($page - 1) * 12;        
        $query = "SELECT p.ID AS id, p.post_title AS title, p.post_content AS content, p.post_date AS date, pi.guid AS img, pi.ID AS img_id, u.user_nicename AS author FROM $wpdb->posts p LEFT JOIN $wpdb->postmeta pm ON (p.ID = pm.post_id AND pm.meta_key = '_thumbnail_id' ) LEFT JOIN $wpdb->posts pi ON (pm.meta_value = pi.ID) LEFT JOIN $wpdb->users u ON (p.post_author = u.ID) WHERE p.post_type = %s AND p.post_title LIKE %s  AND p.post_status='publish' LIMIT %d, 12";
        $query_count = "SELECT COUNT(*) FROM $wpdb->posts p WHERE p.post_type = %s AND p.post_title LIKE %s AND p.post_status='publish'";
        $count = $wpdb->get_var( $wpdb->prepare( $query_count,  [ $post_type, $search ]) );
        $posts = $wpdb->get_results( $wpdb->prepare( $query,  [ $post_type, $search, $offset ]) );
        if (!$posts) {
            $posts = [];
        }        
        foreach($posts as $post) {
            $post->id = (int)$post->id;
            $post->link = get_edit_post_link($post->id);
            $post->title =  $post->title;
            $post->img = imageCacheResizeUrl(wp_get_attachment_url( (int)$post->img_id ), 250, 250);            
            $post->date = date_format( date_create_from_format('Y-m-d H:i:s', $post->date) , 'd M \'y' );
            $post->content = (string)substr(preg_replace('/(<[^>]*>)/', "", str_replace(['&lt;','&gt;'], ['<','>'], $post->content )), 0, 300);
        }
        wp_send_json_success([ 'posts' => $posts, 'page' => $page, 'total' => (int)ceil($count/12), 'query'=>$query, 'query_count'=>$query_count ], 200);
    }        
    
    function actionSaveSettings() {
        if (!wp_doing_ajax()) {
            wp_die(__('Access violation error', 'wpci'), __('Error', 'wpci'));
        }
        if (!isset($_POST['_wpnonce']) || !isset($_POST['__wpci_options']) || !wp_verify_nonce($_POST['_wpnonce'], 'wpci_save_settings')) {
            wp_send_json_error([ 'msg' => __('Error Saving text', 'wpci') ]);
        }
        update_option('__wpci_options', $_POST['__wpci_options'], true);
        wp_send_json_success([ 'msg' => __('Settings updated successfully!!!', 'wpci') ]);
    }

    function actionUpdateConfig() { 
        if (!wp_doing_ajax()) {
            wp_die(__('Access violation error', 'wpci'), __('Error', 'wpci'));
        }
        if (!isset($_POST['_wpnonce']) || !wp_verify_nonce($_POST['_wpnonce'], 'wpci_update_config')) {
            wp_send_json_error([ 'msg' => __('Error updating config', 'wpci') ]);
        }
        if (!current_user_can('activate_plugins')) {
            wp_send_json_error([ 'msg' => __('You do not have sufficient permissions', 'wpci') ]);
        }
        $plugin = Plugin::getInstance();
        $json = WPCI_ROOT  . 'config.json';
        if (!file_exists($json)) {
            wp_send_json_error([ 'msg' => __('Error! Config file not found', 'wpci') ]);
        }
        $config_json = file_get_contents($json);
        $config  = json_decode($config_json, true);
        if (!$config || !$plugin->validateConfig($config)) {
            wp_send_json_error([ 'msg' => __('Error! Config is invalid', 'wpci') ]);
        }
        update_option('__wpci_config', json_encode($config), true);
                
        wp_send_json_success([ 'msg' => __('Config file updated successfully!!!', 'wcpi') ]);        
    }
    
    function actionUploadConfig() { 
        if (!wp_doing_ajax()) {
            wp_die(__('Access violation error', 'wpci'),  __('Error', 'wpci'));
        }
        if (!isset($_POST['_wpnonce']) || !wp_verify_nonce($_POST['_wpnonce'], 'wpci_upload_config')) {
            wp_send_json_error([ 'msg' => __('Error updating config', 'wpci') ]);
        }
        if (!current_user_can('activate_plugins')) {
            wp_send_json_error([ 'msg' => __('You do not have sufficient permissions', 'wpci') ]);
        }
        $plugin = Plugin::getInstance();
        if (!isset($_FILES['config_file']['tmp_name']) || !file_exists($_FILES['config_file']['tmp_name'])) {
            wp_send_json_error([ 'msg' => __('Error! Config file not found') ]);
        }
        $config_json = file_get_contents($_FILES['config_file']['tmp_name']);
        $config  = json_decode($config_json, true);
        if (!$config || !$plugin->validateConfig($config)) {
            wp_send_json_error([ 'msg' => __('Error! Config is invalid', 'wpci') ]);
        }
        update_option('__wpci_config', json_encode($config), true);
                
        wp_send_json_success([ 'msg' => __('Config file updated successfully!!!', 'wcpi') ]);        
    }    
    
}