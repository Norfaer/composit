<?php

namespace composit\core;

trait Base {
    final public function __construct($args = []) {
        $public = get_object_vars($this);
        foreach ($args as $key => $val) {
            if(key_exists( $key, $public)) {
                $this->$key = is_array($this->$key) ? array_replace_recursive($this->$key, $val): $val;                        
            }
        }
        $this->init();
    }
    
    public function __get($name) 
    {
        return $this->{'get' . $name }();
    }
    
    public function init(){}
}