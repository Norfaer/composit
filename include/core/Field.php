<?php
namespace composit\core;

use composit\core\View;

/**
 * This is the class for displaying data fields.
 * @property mixed $value - current field value
 * @property mixed $default - default field value
 * @property string $id - html id attribute of a field
 * @property string $label - html label for input element
 * @property string $name - html name attribute for input element
 * @property string $description - field description
 * @property composit\core\View $view - view instance
 */
class Field {
    use Base;
    
    static $uniqueId = 0;
    
    public $object = 'post';
    public $object_id = null;
    
    public $value = '';
    public $params = [];
    public $list = [];
    public $children = [];
    public $name = '';
    public $id = '';
    public $label = '';   
    public $width = 'col-sm-12';
    public $description = '';
    public $multiple = false;
    public $default = '';
    public $type = ''; 
    public $collection = false;    
    
    private $_v = null;

    public function init() {
        self::$uniqueId++;
        if ($this->collection) {
            $this->params['required'] = false;
            $this->value = '';
        } else if (empty($this->value)) {
            $value = get_metadata($this->object, $this->object_id, $this->name, !($this->multiple || $this->type==='collection') );        
            $this->value = ($this->multiple || $this->type==='collection') ? ($value ? $value : []) : $value;  
        }
    }
    
    public function save() {
        if ($this->type === 'tabs') {
            foreach($this->children as $tab) {
                foreach($tab['children'] as $child) {
                    $child['object_id'] = $this->object_id;
                    $child['object'] = $this->object;
                    (new Field($child))->save();
                }
            }
            return;
        }
        $key = $this->name;
        delete_metadata($this->object, $this->object_id, $key);
        if (!isset($_POST[$key])) {
          return;
        }
        $value = $this->multiple ? explode(',', $_POST[$key]) : $_POST[$key];  
        if (is_array($value) && ( $this->multiple || $this->type==='collection' )) {
            $value = array_filter($value);
            foreach($value as $single) {
              add_metadata( $this->object, $this->object_id, $key, $single, false );               
            }
        }
        else {
          add_metadata( $this->object, $this->object_id, $key, $value, true );
        }             
    }
        
    public function render() {
        if (!$this->id) {
            $this->id = 'wpci-input-'.self::$uniqueId;
        }
        if($this->type && method_exists($this, "render{$this->type}")) {           
            $this->{'render'.$this->type}();
        }
    }
    
    /**
     * Render input form control
     *
     * @since 1.0.0
     *
     * @param composit\core\Field     $this  The composit\core\Field instance, passed by reference.
     * 
     * @param string    $name  The name html attribute of the input,
     * @param string    $label The label for the input,
     * @param string    $width The width of the column, by default 'col-xs-12'
     * @param string    $deescription Input Description
     * 
     * @param array     $params  Additional settings:
     *                      'placeholder' - Input placeholder
     *                      'type'        - Type of input, one of: email, text, 
     *                                      tel, number
     *                      'required'    - Tells if input is required to be filled
     *                                      with non-empty value 
     *                      'validator'   - If set, contains RegEx for validating input
     *                      'icon'        - Input icon 
     */
    private function renderInput() {
        $this->params = wp_parse_args($this->params, [
            'type' => 'text',
            'placeholder' => $this->label,
            'required' => false,
            'validator' => '',
            'icon' => ''
        ]);
        echo $this->view->render('fields/input', (array)$this);                
    }
    
    /**
     * Render checkbox form control
     *
     * @since 1.0.0
     *
     * @param composit\core\Field     $this  The composit\core\Field instance, passed by reference.
     * 
     * @param string    $name  The name html attribute of the datepicker,
     * @param string    $label The label for the datepicker,
     * @param string    $width The width of the column, by default 'col-xs-12'
     * 
     * @param array     $params  Additional settings:
     *                      'format'   - date format to save and to store
     *                      'required' - is field required
     */    
    private function renderDatepicker() {
        wp_enqueue_script('wpci-field-datepicker');
        $this->params = wp_parse_args($this->params, [            
            'placeholder' => '',
            'required' => false,
            'dateFormat' => 'yy-mm-dd',
            'time' => false            
        ]);
        echo $this->view->render('fields/datepicker', (array)$this);        
    }
    
    /**
     * Render checkbox form control
     *
     * @since 1.0.0
     *
     * @param composit\core\Field     $this  The composit\core\Field instance, passed by reference.
     * 
     * @param string    $name  The name html attribute of the checkbox,
     * @param string    $label The label for the checkbox,
     * @param string    $width The width of the column, by default 'col-xs-12'
     * 
     */    
    private function renderCheck() {
        echo $this->view->render('fields/check', (array)$this);        
    }

    /**
     * Render select form control
     *
     * @since 1.0.0
     * @access public
     *
     * @param composit\core\Field    $this  The composit\core\Field instance, passed by reference. 
     * 
     * @param string    $name     The name html attribute of the select control,
     * @param string    $label    The label for the select dropdown,
     * @param string    $width    The width of the column, by default 'col-xs-12'
     * @param array     $list     The list of values for <option> tags in format, 'value' => 'Value Label'
     * @param string    $default  The default value of select dropdown
     * @param array     $params   Additional parameters:
     *                      'prompt'   - dummy option prompting for select
     *                      'required' - if field is required
     */     
    private function renderSelect() {        
        wp_enqueue_script('field-select');
        if (empty($this->value)) {
            $this->value = $this->list[(int)$this->default]['key'];
        }
        $this->params = wp_parse_args($this->params, [
            'prompt' => __('--Select a value--', 'wpci'),
            'required' => false
        ]);        
        echo $this->view->render('fields/select', (array)$this);                        
    }

    /**
     * Render radiobutton form control
     *
     * @since 1.0.0
     *
     * @param composit\core\Field     $this  The composit\core\Field instance, passed by reference.
     * 
     * @param string    $name     The name html attribute of the radiobutton,
     * @param string    $label    The label for the radiobutton,
     * @param string    $width    The width of the column, by default 'col-xs-12'
     * @param array     $list     The list of values in format, 'value' => 'Value Label'
     * @param string    $default  The default value of radio switch
     */        
    private function renderRadio() { 
        if (empty($this->value)) {
            $this->value = $this->list[(int)$this->default]['key'];
        }
        $this->params = wp_parse_args($this->params, [ 'required' => false ]);   
        echo $this->view->render('fields/radio', (array)$this);                
    }
    
    /**
     * Render radiobutton form control
     *
     * @since 1.0.0
     *
     * @param composit\core\Field     $this  The composit\core\Field instance, passed by reference.
     * 
     * @param string    $name     The name html attribute of the radiobutton,
     * @param string    $label    The label for the radiobutton,
     * @param string    $width    The width of the column, by default 'col-xs-12'
     * @param array     $params  Additional settings:
     *                      'from' - minimum value
     *                      'to' - maximum value
     *                      'step' - starting value 
     */        
    private function renderRange() {
        $this->params = wp_parse_args($this->params, [
            'from' => 0,
            'to' => 100,
            'step' => 1            
        ]);        
        echo $this->view->render('fields/range', (array)$this);        
    }

    
    /**
     * Render input-mask form control
     *
     * @since 1.0.0
     *
     * @param composit\core\Field     $this  The composit\core\Field instance, passed by reference.
     * 
     * @param string    $name  The name html attribute of the input,
     * @param string    $label The label for the input,
     * @param string    $width The width of the column, by default 'col-xs-12'
     * @param string    $deescription Input Description
     * 
     * @param array     $params  Additional settings:
     *                      'placeholder' - Input placeholder
     *                      'required'    - Tells if input is required to be filled
     *                                      with non-empty value 
     *                      'pattern'     - Mask
     *                      'icon'        - Input icon 
     */
    private function renderMask() {
        wp_enqueue_script('wpci-mask');     
        $this->params = wp_parse_args($this->params, [
            'pattern' => '',
            'required' => false,
            'placeholder' => '_',
            'icon' => ''
        ]);      
        echo $this->view->render('fields/mask', (array)$this);
    }
    
        
    /**
     * Render clorpicker form control
     *
     * @since 1.0.0
     *
     * @param composit\core\Field     $this  The composit\core\Field instance, passed by reference.
     * 
     * @param string    $name  The name html attribute of the colorpicker,
     * @param string    $label The label for the colorpicker,
     * @param string    $width The width of the column, by default 'col-xs-12'
     * 
     */      
    private function renderColor() {
        wp_enqueue_script( 'wp-color-picker' );
        echo $this->view->render('fields/color', (array)$this);                
    }

    /**
     * Render image/gallery selection tool
     *
     * @since 1.0.0
     *
     * @param composit\core\Field     $this  The composit\core\Field instance, passed by reference.
     * 
     * @param string    $name     The name html attribute of the hidden input,
     * @param string    $label    The label for the gallery,
     * @param string    $width    The width of the column, by default 'col-xs-12'
     * @param bool      $multiple Allow image multiselect (gallery)
     * @param array     $params  Additional settings:
     *                      'placeholder' - placeholder text
     * 
     */            
    private function renderImage() {
        wp_enqueue_media();
        wp_enqueue_script('wpci-field-image');        
        $this->params = wp_parse_args($this->params, [
            'placeholder' => __('Choose Image','wpci'),
        ]);            
        if ($this->multiple) {
            $this->value = is_array($this->value) ? $this->value : explode(',', $this->value);
        }
        echo $this->view->render($this->multiple ? 'fields/image-multi' : 'fields/image-single', (array)$this);
    }

    
    
    /**
     * Render post reference widget
     *
     * @since 1.0.0
     *
     * @param composit\core\Field     $this  The composit\core\Field instance, passed by reference.
     * 
     * @param string    $name     The name html attribute of the hidden input,
     * @param string    $label    The label for the post reference,
     * @param string    $width    The width of the column, by default 'col-xs-12'
     * @param bool      $multiple Allow multiple selection of posts
     * @param array     $params  Additional settings:
     *                      'post_type' - array of supported post types in format: "post_type" => "Post type name"
     *                      'filters'   - array of taxonomy filters in format: "post_tpye" => [ 'tax1', 'tax2', ... ] 
     */                
    private function renderPostRef() {
        wp_localize_script('wpci-field-postref', 'Wpci_Postref_I18n', [
            'choose' => __('Choose', 'wpci'),
            'nof' => __('Nothing found', 'wpci'),
            'prev' => __('Previous', 'wpci'),
            'next' => __('Next', 'wpci'),
            'cancel' => __('Cancel', 'wpci'),
            'search' => __('Search...', 'wpci'),
            'remove' => __('Remove', 'wpci'),
            'visit' => __('Visit', 'wpci')
        ]);        
        wp_enqueue_script('wpci-field-postref');        
        global $wpdb;        
        $this->params['posts'] = $this->params['postTypes'];
        $this->params = wp_parse_args($this->params, [
            'action' => get_admin_url( null ,'admin-ajax.php' ) .'?action=get_posts',
            'nonce' => wp_create_nonce( 'getpost' ),
            'ref' => esc_attr( wp_unslash( $_SERVER['REQUEST_URI'] ) ),
            'filters' => [],
            'postTypes' => []
        ]);        
        foreach ($this->params["filters"] as $post_type => $filters) {
            foreach($filters as $index => $taxonomy) {
                $query = "SELECT t.term_id, t.name, t.slug, tt.count FROM $wpdb->terms t INNER JOIN $wpdb->term_taxonomy tt ON (t.term_id = tt.term_id) WHERE tt.taxonomy='$taxonomy'";            
                $terms = $wpdb->get_results(  $query );
                $this->params["filters"][$post_type][$taxonomy]['model'] = 0;
                $this->params["filters"][$post_type][$taxonomy]['list'] = $terms;                
                unset($this->params["filters"][$post_type][$index]);
            }
        }
        $pt = [];
        foreach ($this->params['postTypes'] as $post_type) {
            $pt[$post_type] = get_post_type_object( $post_type ) ? get_post_type_object( $post_type )->label : '';
        }
        $this->params['postTypes'] = $pt;
        $this->params = safe_json((object)$this->params);
        $posts  = [];
        if ($this->value) {
            $ids = $this->multiple ? (is_array($this->value) ? implode(',', $this->value ) : $this->value ) :  $this->value ;
            $query = "SELECT p.ID AS id, p.post_title AS title, p.post_content AS content, p.post_date AS date, pi.guid AS img, pi.ID AS img_id, u.user_nicename AS author FROM $wpdb->posts p LEFT JOIN $wpdb->postmeta pm ON (p.ID = pm.post_id AND pm.meta_key = '_thumbnail_id' ) LEFT JOIN $wpdb->posts pi ON (pm.meta_value = pi.ID) LEFT JOIN $wpdb->users u ON (p.post_author = u.ID) WHERE p.ID IN ({$ids}) AND p.post_status='publish'";
            $posts = $wpdb->get_results(  $query );
            foreach($posts as $post) {
                $post->title = $post->title;
                $post->id = (int)$post->id;
                $post->link =  get_edit_post_link($post->id);
                $post->img = wp_get_attachment_image_src((int)$post->img_id)[0];            
                $post->date = date_format( date_create_from_format('Y-m-d H:i:s', $post->date) , 'd M \'y' );
                $post->content = (string)substr(preg_replace('/(<[^>]*>)/', "", str_replace(['&lt;','&gt;'], ['<','>'], $post->content)), 0, 300);
            }
        }
        $this->value = safe_json($posts ? $posts : []);
        $attr = '';
        $attr = ($this->multiple ? " id='{$this->id}' :allow-multiselect='true' name='{$this->name}'":" :allow-multiselect='false' name='{$this->name}'") ." label='{$this->label}' name='{$this->name}' :params='{$this->params}' :value='{$this->value}'";
        echo "<div class='{$this->width}'><div class='form-group'><div class=\"post-ref\"><Postref {$attr}></Postref></div></div></div>";
    }
    
    
    
    /**
     * Render google-map widget
     *
     * @since 1.0.0
     *
     * @param composit\core\Field     $this  The composit\core\Field instance, passed by reference.
     * 
     * @param string    $name     The name html attribute of the hidden input,
     * @param string    $label    The label for the gallery,
     * @param string    $width    The width of the column, by default 'col-xs-12'
     * @param array     $params  Additional settings:
     *                      'zoom' - zoom level (0-21)
     *                      'centerx' - longtitude of map center
     *                      'centery' - lattitude of map center
     *                      'markers' - array of map Markers serialized to string: 'x,y;x,y;x,y...'  
     */            
    private function renderMapRef() {        
        $this->params = wp_parse_args($this->params, [
            'centerx'  => -180,
            'centery'  => 0,
            'zoom'    => 5,
            'seach' => true,
            'country' => substr(get_locale(), -2)
        ]);
        $this->value = wp_parse_args($this->value, [
            'centerx'  => $this->params['centerx'],
            'centery'  => $this->params['centery'],
            'zoom'    => $this->params['zoom'],
            'markers' => ''
        ]);        
        wp_enqueue_script('wpci-field-mapref');
        echo $this->view->render('fields/mapref', (array)$this);
    }

    /**
     * Render input form control
     *
     * @since 1.0.0
     *
     * @param composit\core\Field     $this  The composit\core\Field instance, passed by reference.
     * 
     * @param string    $name  The name html attribute of the teextarea,
     * @param string    $label The label for the textarea,
     * @param string    $width The width of the column, by default 'col-xs-12'
     * 
     * @param array     $params  Additional settings:
     *                      'placeholder' - Input placeholder
     *                      'wysiwyg'     - If true - then tinimce editor is used,
     *                                      else - usual textarea
     *                      'rows'        - Number of rows to show
     */    
    private function renderEditor() {
        $this->params = wp_parse_args($this->params, [
            'placeholder' => '',
            'type'     => 'textarea',
            'rows'        => 10
        ]);        
        echo "<div class='{$this->width}'><div class='form-group' data-name-template='{$this->name}'><label for='{$this->id}'>{$this->label}</label>";
        if ($this->params['type'] === 'wysiwyg') {
            wp_editor( htmlspecialchars_decode( $this->value), $this->id,  [
                'wpautop'             => false,
                'media_buttons'       => false,
                'default_editor'      => '',
                'drag_drop_upload'    => false,
                'textarea_name'       => $this->name,
                'textarea_rows'       => $this->params['rows'],
                'tabindex'            => '',                
                'editor_css'          => '',
                'editor_class'        => 'i18n-multilingual',
                'teeny'               => false,
                'dfw'                 => false,
                '_content_editor_dfw' => false,
                'tinymce'             => true,
                'quicktags'           => true
            ] );
        }
        else {
           echo "<textarea  autocomplete='off' id='{$this->id}'  class='form-control i18n-multilingual' data-name-template='{$this->name}' name='{$this->name}' rows='{$this->params['rows']}' placeholder='{$this->params['placeholder']}'>{$this->value}</textarea>";
        }
        echo "</div></div>";
    }
    
    
    private function renderCollection() {
        $this->params = wp_parse_args($this->params, [
            'representation' => $this->label . ' #%i',
        ]);        
        wp_enqueue_script('wpci-field-multicollection');
        echo $this->view->render('fields/collection', (array)$this); 
    }

    /**
     * Render tabs 
     *
     * @since 1.0.0
     *
     * @param composit\core\Field     $this  The composit\core\Field instance, passed by reference.
     * 
     * @param array     $children   Array of tabs 
     *                      'label' - Label of tab
     *                      'icon'  - Icon of tab
     *                      'children' - Array containing child controls
     */      
    private function renderTabs() {
        echo '<div class="col-sm-12"><ul class="nav nav-tabs nav-tabs-neutral justify-content-center" role="tablist">';
        foreach($this->children as $index => $tab) {
            $tab[$index] = wp_parse_args($tab, [ 'children' => [], 'label' => 'Tab '. $index, 'icon' => '' ]);        
            echo '<li class="nav-item"><a class="nav-link' . ($index ? '':' active') . '" data-toggle="tab" href="#'.$this->id.'-' . $index . '" role="tab" aria-expanded="'.($index ? 'false':'true').'">'.( $tab['icon'] ? '<i class="fa '.$tab['icon'].'">' : '' ).'</i> ' . $tab['label'] . '</a></li>';
        }
        echo '</ul>';        
        echo '<div class="card-body"><div class="tab-content">';
        foreach($this->children as $index => $tab) {
            echo '<div id="'.$this->id.'-' . $index . '" class="tab-pane' . ($index ? '':' active') . '" role="tabpanel" area-expanded="' . ($index ? 'false':'true') . '"><div class="container-fluid"><div class="row">';
            if(!empty($tab['children'])) {
                foreach($tab['children'] as $child) {
                    $child['object_id'] = $this->object_id;
                    $child['object'] = $this->object;
                    (new Field($child))->render($this->object_id);
                }
            }
            echo '</div></div></div>';
        }        
        echo '</div></div></div>';
    }
    
    public function getView() {
        if (!$this->_v) {
            $this->_v = new View();
        }
        return $this->_v;
    }
}