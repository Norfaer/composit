<?php

namespace composit\core;

trait BaseSingleton
{
    protected static $_instance;
    final private function __construct($args = []) {
        $public = get_object_vars($this);
        foreach ($args as $key => $val) {
            if(key_exists( $key, $public)) {
                $this->$key = is_array($this->$key) ? array_replace_recursive($this->$key, $val): $val;                        
            }
        }
        $this->init();
    }    
    public function init(){}
    final public static function getInstance($args = []) {return isset(static::$_instance) ? static::$_instance : static::$_instance = new static($args);}
    final private function __wakeup() {}
    final private function __clone() {}    
    final public function __get($name) 
    {
        return $this->{'get' . $name }();
    }
    
}