<?php

namespace composit\core;

class Taxonomy {
    
    use Base;
    
    public $slug = '';    
    public $posts = [];
    public $multiple = true;
    public $params = [
        'public'=> true,
        'publicly_queryable' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => false,
        'rest_controller_class' => 'WP_REST_Terms_Controller',
        'show_tagcloud' => true,
        'show_in_quick_edit' => true,
        'meta_box_cb' => null,
        'show_admin_column'=> false,
        'description' => '',        
        'hierarchical' => true,        
    ];
    public $meta = [];
    public $columns = [];
    
    private function registerMetaColumn($field) {
      if (!empty($field['column']))
        $this->columns[$field['name']] = [
          'name' => $field['name'],
          'type' => $field['type'],
          'label' => $field['label']
        ];              
    }
    
    private function registerMetaColumns() {      
      foreach ($this->meta as $meta_box)
        foreach($meta_box['fields'] as $field) {
          $this->registerMetaColumn($field);
        }
    }    
   
    public function init() {
        add_action( 'init', [$this, 'registerTaxType'], 0 );
        if (is_admin()) {
            if (!empty($_REQUEST['taxonomy']) && $_REQUEST['taxonomy'] == $this->slug) {
                add_action('load-edit-tags.php', [$this, 'registerEditor'], 1);
                add_action('load-term.php', [$this, 'registerEditor'], 1);
                add_filter('manage_edit-'.$this->slug.'_columns', [$this,'registerColumns']);
                add_filter('manage_'.$this->slug.'_custom_column', [$this,'getColumnValue'], 10, 3);                
                $this->registerMetaColumns();                                
            }
        }                
    }    

    public function registerEditor() {
        if (!empty($this->meta)) {
            add_action($this->slug.'_edit_form', [$this ,'editFields']);
            add_action($this->slug.'_add_form_fields', [$this ,'addFields']);
        }
    }

    public function registerTaxType() {
	register_taxonomy( $this->slug, $this->posts, $this->params );
        if (!empty($this->meta)) {
            add_action('edited_'.$this->slug, [$this,'saveMeta'],10,1);
            add_action('created_'.$this->slug, [$this,'saveMeta'],10,1);
        }
    }   

    public function addFields($taxonomy_name) {
        echo '<div class="bs-root"><div class="taxonomy-fields"><div class="row">';
        foreach ($this->meta as $meta_name => $meta_params) {
            foreach ($meta_params['fields'] as $field) {
                $field['object'] = 'term';
                $field['object_id'] = null;
                (new Field($field))->render();            
            }
        }
        echo '</div></div></div>';        
    }    
    
    public function editFields( $term ) {
        echo '<div class="bs-root"><div class="taxonomy-fields"><div class="row">';
        foreach ($this->meta as $meta_name => $meta_params) {
            foreach ($meta_params['fields'] as $field) {
                $field['object'] = 'term';
                $field['object_id'] = $term->term_id;
                (new Field($field))->render();            
            }
        }
        echo '</div></div></div>';        
    }
    
    public function registerColumns($columns) {
        unset($columns['posts']);
        foreach($this->columns as $name => $column) {
          $columns[$name] = $column['label'];
        }
        $columns['posts'] = __('Posts');
        return $columns;
    }

    public function getColumnValue($unused, $name, $term_id) {
        if (key_exists($name, $this->columns)) {
            $value = get_term_meta($term_id, $name, true);
            switch($this->columns[$name]['type']) {
                case 'input':
                case 'datepicker':
                case 'mask':
                case 'range':
                    echo $value;
                    break;   
                case 'color': 
                    echo '<div style="background-color: '.$value.'; width:16px; height:16px;"></div>';
                    break;
                case 'check':
                    echo $value === 'on' ? __('Yes', 'wpci') : __('No', 'wpci');
                    break;
                case 'image':
                    echo (int)$value ? '<img src="'.imageCacheResizeId((int)$value, 50, 50).'" width="50" height="50">' : __('No', 'wpci');
                    break;
            }
        }
    }
    
    public function saveMeta($term_id) {   
        if ( ! current_user_can('edit_term', $term_id) ) return;
        foreach ($this->meta as $meta_name => $meta_params) {        
            foreach ($meta_params['fields'] as $field) {
                $field['object'] = 'term';
                $field['object_id'] = $term_id;
                (new Field($field))->save();            
            }        
        }
    }

}