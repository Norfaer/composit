<?php

namespace composit\core;

class Post  {
    
    use Base;
    
    public $slug = '';    
    public $params = [
	'public'             => true,
	'publicly_queryable' => true,
	'show_ui'            => true,
	'show_in_menu'       => true,
	'query_var'          => true,
	'capability_type'    => 'post',
	'has_archive'        => true,
	'hierarchical'       => false,
	'menu_position'      => null,
	'supports'           => [ 'title' ]	        
    ];
    public $columns = [];    
    public $meta = [];
    
    private function registerMetaColumn($field) {
      if (!empty($field['column']))
        $this->columns[$field['name']] = [
          'name' => $field['name'],
          'type' => $field['type'],
          'label' => $field['label'],
          'values' => isset($field['list']) ? array_column($field['list'], 'value', 'key') : [] 
        ];              
    }
    
    private function registerMetaColumns() {
      foreach ($this->meta as $meta_box)
        foreach($meta_box['fields'] as $meta) {
          if ($meta['type'] === 'tabs') {
            foreach($meta['children'] as $tab)
              foreach($tab['children'] as $field)
                $this->registerMetaColumn($field);
          }
          else {
            $this->registerMetaColumn($meta);
          }
        }
    }
    
    final public function init() {
        add_action('init', [ $this, 'registerPost' ]);
        add_action('save_post_' . $this->slug, [ $this, 'save' ], 1, 3);
        add_action('add_meta_boxes_'.$this->slug, [ $this, 'registerMetaBoxes' ], 10, 1);        
        if (is_admin()) {
            if (!empty($_REQUEST['post_type']) && $_REQUEST['post_type'] == $this->slug) {
                add_action('load-edit.php', [$this, 'registerListView'], 1);                
                add_action('wp_ajax_inline-save', [$this, 'registerListView'], 1);
                $this->registerMetaColumns();
            }
        }
    }

    final public function registerPost() {
        if (!in_array($this->slug, [ 'post', 'page' ])) {
            register_post_type($this->slug, $this->params);
        }
    }

    final public function registerMetaBoxes($post) {
        foreach($this->meta as $meta_name => $meta_params) {
            $template = get_post_meta( $post->ID, '_wp_page_template', true );
            if (!empty($meta_params['restrict_by_id']) && !(is_array($meta_params['restrict_by_id']) ?  in_array($post->ID, $meta_params['restrict_by_id']) : $post->ID ==  $meta_params['restrict_by_id']))            
                continue;
            if (!empty($meta_params['restrict_by_template']) && $this->slug === 'page' && !(is_array($meta_params['restrict_by_template']) ?  in_array($template, $meta_params['restrict_by_template']) : $template ==  $meta_params['restrict_by_template']))            
                continue;
            add_meta_box('wpci-' . $this->slug . '-' . $meta_name, $meta_params['title'], [ $this, 'metaBox'], null, empty($meta_params['context']) ? 'normal' : $meta_params['context'], $meta_params['priority'] ? $meta_params['priority'] : 'default', $meta_params);            
        }        
    }
    
    public function metaBox($post, $cb) {
        echo '<div class="bs-root"><div class="row">';
        wp_nonce_field( WPCI_ROOT_URL . '/save-' . $post->post_type, 'wpci_save_meta' );
        foreach($cb['args']['fields'] as $field) {
            $field['object_id'] = $post->ID;
            $field['object'] = 'post';
            (new Field($field))->render();
        }
        echo '</div></div>';
    }

    final public function registerListView() {
        add_filter('manage_' . $this->slug . '_posts_columns', [$this, 'registerColumns'], 1);
        add_filter('manage_edit-' . $this->slug . '_sortable_columns', [$this, 'registerSortableColumns'], 1);
        add_action('manage_' . $this->slug . '_posts_custom_column', [$this, 'getColumnValue'], 1, 2);
        add_action('restrict_manage_posts', [$this, 'registerTaxFilter'], 1, 1);
        add_action('pre_get_posts', [$this, 'sortQuery'], 2);
    }
    
    final public function save($post_id, $post, $update) {       
        if (!isset($_POST['wpci_save_meta']) ||
            !wp_verify_nonce($_POST['wpci_save_meta'], WPCI_ROOT_URL . '/save-' . $this->slug) ||
            !current_user_can('edit_post', $post_id) ||
            wp_is_post_autosave($post_id) ||
            wp_is_post_revision($post_id))
        {
            return;
        }
        foreach ($this->meta as $meta) {
            foreach($meta['fields'] as $field) {
                $field['object'] = 'post';
                $field['object_id'] = $post_id;
                (new Field($field))->save();
            }
        }
    }
    
    final public function registerColumns($columns) {
        unset($columns['date']);        
        foreach($this->columns as $name => $column) {
          $columns[$name] = $column['label'];
        }                
        $columns['date'] = __('Date');        
        return $columns;
    }

    public function registerSortableColumns($columns) {
        foreach($this->columns as $name => $column) {
            if ($column['type']!=='image') {
                $columns[$name] = $name;
            }
        }
        return $columns;
    }

    final public function getColumnValue($name, $post_id) {
        if (key_exists($name, $this->columns)) {
            $value = get_post_meta($post_id, $name, true);
            switch($this->columns[$name]['type']) {
                case 'input':
                case 'datepicker':
                case 'mask':
                case 'range':
                    echo $value;
                    break;   
                case 'color': 
                    echo '<div style="background-color: '.$value.'; width:16px; height:16px;"></div>';
                    break;
                case 'check':
                    echo $value === 'on' ? __('Yes', 'wpci') : __('No', 'wpci');
                    break;
                case 'radio':
                case 'select':
                    echo empty($this->columns[$name]['values'][$value]) ? '' : $this->columns[$name]['values'][$value];
                    break;
                case 'image':
                    echo (int)$value ? '<img src="'.imageCacheResizeId((int)$value, 50, 50).'" width="50" height="50">' : __('No', 'wpci');
                    break;
            }
        }
    }

    final public function sortQuery($query) {
        if ($query->is_main_query() && ( $orderby = $query->get('orderby') ) && array_key_exists($orderby, $this->columns)) {
            $query->set('meta_key', $orderby);
            $query->set('orderby', 'meta_value');
        }
    }

    public function registerTaxFilter($post_type) {
        global $typenow;
        if ($typenow == $this->slug) {
            $taxonomies = get_object_taxonomies($this->slug, 'objects');
            foreach($taxonomies as $taxonomy) {
                $selected      = isset($_REQUEST[$taxonomy->name]) ? $_REQUEST[$taxonomy->name] : '';
                wp_dropdown_categories([
                        'show_option_all' => __('All','wpci') . ' ' . $taxonomy->label,
                        'taxonomy'        => $taxonomy->name,
                        'name'            => $taxonomy->name,
                        'orderby'         => 'name',
                        'selected'        => $selected,
                        'show_count'      => true,
                        'hide_empty'      => false,
                        'value_field'     => 'slug',
                        'hierarchical'    => (int)$taxonomy->hierarchical
                ]);
            }
        }
    }

}
