<?php
namespace composit\core;

use composit\core\View;

abstract class Module {
    use BaseSingletonFactory;
    
    public $path;
    public $name;
    public $plugin;
    public $assets_url;
    
    private $_v = null;
    
    abstract public function adminHooks();
    abstract public function commonHooks();
    abstract public function frontHooks();
    abstract public function initModule();

    final private function init() {
        add_action('init', [ $this, 'commonHooks']); 
        add_action('admin_init', [ $this, 'adminHooks']);
        add_action('template_redirect', [$this, 'frontHooks']);
        $this->plugin = Plugin::getInstance();
        $this->path = WPCI_ROOT . 'include' . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . strtolower($this->name) . DIRECTORY_SEPARATOR;
        $this->url = WPCI_ROOT_URL . 'include/modules/' . strtolower($this->name) ;
        $this->assets_url = $this->url . '/assets';
        $this->initModule();
    }
    
    final public function getView() {
        if (!$this->_v){
            $this->_v = new View(['path' => [ $this->path . 'views' . DIRECTORY_SEPARATOR ]]);
        }
        return $this->_v;
    }    
}