<?php

namespace composit\core;

trait BaseSingletonFactory
{
    protected static $_instances = [];
    final private function __construct($args = []) {
        $public = get_object_vars($this);
        foreach ($args as $key => $val) {
            if(key_exists( $key, $public)) {
                $this->$key = is_array($this->$key) ? array_replace_recursive($this->$key, $val): $val;                        
            }
        }
        $this->init();
    }    
    public function init(){}
    final public static function getInstance($args = []) {
        $class = get_called_class();
        return isset(self::$_instances[$class]) ? self::$_instances[$class] : self::$_instances[$class] = new $class($args);        
    }
    final private function __wakeup() {}
    final private function __clone() {}    
    final public function __get($name) 
    {
        return $this->{'get' . $name }();
    }

}